<?php

$theme_uri = get_template_directory_uri();

/* General */
$this->sections[] = array(
  'icon'      => 'ti-settings',
  'title'     => __( 'General', 'dylan_addons' ),
  'fields'    => array(
    array(
      'id' => 'nav_position',
      'title' => __('Navbar Position', 'dylan_addons'),
      'type' => 'select',
      'options' => array(
        'top' => 'Top',
        'left' => 'Left',
      ),
      'default' => 'top'
    ),
    array(
      'id'  => 'logo_light',
      'title' => __( 'Light Logo', 'dylan_addons' ),
      'subtitle'  => __( 'Upload your .png or .jpg logo', 'dylan_addons' ),
      'type'  => 'media',
      'default' => array(
        'url' => $theme_uri . '/assets/images/logo_light.png'
      )
    ),
    array(
      'id'  => 'logo_dark',
      'title' => __( 'Logo', 'dylan_addons' ),
      'subtitle'  => __( 'Upload your .png or .jpg logo', 'dylan_addons' ),
      'type'  => 'media',
      'default' => array(
        'url' => $theme_uri . '/assets/images/logo_dark.png'
      )
    ),
    array(
      'id'       => 'smooth_scroll',
      'type'     => 'switch',
      'title'    => __('Smooth Scroll', 'dylan_addons'),
      'subtitle' => __('Enable Smooth Scrolling, it\'s on!', 'dylan_addons'),
      'default'  => true,
    ),
    array(
      'id'       => 'hide_preloader',
      'type'     => 'switch',
      'title'    => __('Preloader', 'dylan_addons'),
      'subtitle' => __('Look, it\'s on!', 'dylan_addons'),
      'default'  => true,
    ),
    array(
      'id'       => 'hide_search_form',
      'type'     => 'switch',
      'title'    => __('Header Search Form', 'dylan_addons'),
      'subtitle' => __('Warning: if set to off, individual page settings will be overridden.', 'dylan_addons'),
      'default'  => true,
    ),
    array(
      'id'       => 'hide_cart',
      'type'     => 'switch',
      'title'    => __('Header Shopping Cart', 'dylan_addons'),
      'subtitle' => __('Warning: if set to off, individual page settings will be overridden.', 'dylan_addons'),
      'default'  => true,
    ),
    array(
      'id'      =>'google_maps_api_key',
      'type'    => 'text',
      'title'   => __('Google Maps Api Key', 'comet_addons'),
      'default' => '',
    ),
  )
);

/* Styling */
$this->sections[] = array(
  'icon'      => 'ti-pencil',
  'title'     => __( 'Styling', 'dylan_addons' ),
  'fields'    => array(
    array(
      'id'    => 'primary_color',
      'type'  => 'color',
      'title' => __('Primary Color', 'dylan_addons'),
      'subtitle'  => __('Pick the primary color for the theme', 'dylan_addons'),
      'transparent' => false,
      'default' => '#1b96ed'
    ),
    array(
      'id'    => 'dark_color',
      'type'  => 'color',
      'title' => __('Dark Color', 'dylan_addons'),
      'subtitle'  => __('Pick the dark color for the theme', 'dylan_addons'),
      'description' => __("It's used for dark sections, buttons, and navbars.", 'dylan_addons'),
      'transparent' => false,
      'default' => '#222225'
    ),
    array(
      'id'    => 'text_color',
      'type'  => 'color',
      'title' => __('Text Color', 'dylan_addons'),
      'subtitle'  => __('Pick the text color for the theme', 'dylan_addons'),
      'transparent' => false,
      'default' => '#2d2d2d'
    ),
    array(
      'id' => 'menu_color',
      'title' => __('Navbar Color', 'dylan_addons'),
      'type' => 'select',
      'options' => array(
        'light-menu' => 'Light',
        'dark-menu'  => 'Dark',
      ),
      'default' => 'light'
    ),
    array(
      'id'          => 'primary_font',
      'type'        => 'typography',
      'title'       => __('Primary Font', 'dylan_addons'),
      'subtitle'    => __('Select the primary font for the theme', 'dylan_addons'),
      'description' => __('Leave blank to use default theme fonts.', 'dylan_addons'),
      'google'      => true,
      'fonts'       => array('Helvetica' => 'Helvetica Neue, Helvetica'),
      'font-backup' => false,
      'color'       => false,
      'text-align'  => false,
      'subsets'     => false,
      'line-height' => false,
      'font-size'   => false,
      'output'      => '',
      'units'       =>'px',
    ),
    array(
      'id'          => 'heading_font',
      'type'        => 'typography',
      'title'       => __('Heading Font', 'dylan_addons'),
      'subtitle'    => __('Select the font for the heading tags (H1, H2, H1, H4, H5, H6)', 'dylan_addons'),
      'description' => __('Leave blank to use default theme fonts.', 'dylan_addons'),
      'google'      => true,
      'fonts'       => array('Helvetica' => 'Helvetica Neue, Helvetica'),
      'font-backup' => false,
      'color'       => false,
      'text-align'  => false,
      'subsets'     => false,
      'font-weight' => false,
      'line-height' => false,
      'font-size'   => false,
      'output'      => '',
      'units'       =>'px',
    ),
    array(
      'id'          => 'serif_font',
      'type'        => 'typography',
      'title'       => __('Serif Font', 'dylan_addons'),
      'subtitle'    => __('Select the serif font family.', 'dylan_addons'),
      'description' => __('Leave blank to use default theme fonts.', 'dylan_addons'),
      'google'      => true,
      'fonts'       => array('Helvetica' => 'Helvetica Neue, Helvetica'),
      'font-backup' => false,
      'color'       => false,
      'text-align'  => false,
      'subsets'     => false,
      'line-height' => false,
      'font-size'   => false,
      'output'      => '',
      'units'       =>'px',
    ),
    array(
      'id'          => 'cursive_font',
      'type'        => 'typography',
      'title'       => __('Cursive Font', 'dylan_addons'),
      'subtitle'    => __('Select the cursive font family.', 'dylan_addons'),
      'description' => __('Leave blank to use default theme fonts.', 'dylan_addons'),
      'google'      => true,
      'fonts'       => array('Helvetica' => 'Helvetica Neue, Helvetica'),
      'font-backup' => false,
      'color'       => false,
      'text-align'  => false,
      'subsets'     => false,
      'line-height' => false,
      'font-size'   => false,
      'output'      => '',
      'units'       =>'px',
    ),
    array(
      'id'       => 'custom_css',
      'type'     => 'ace_editor',
      'title'    => __('Custom CSS', 'dylan_addons'),
      'subtitle' => __('Paste your CSS code here.', 'dylan_addons'),
      'mode'     => 'css',
      'theme'    => 'monokai',
      'default'  => "/* Your code here */ "
    ),
  )
);


/* Social Networks */
$this->sections[] = array(
  'icon'      => 'ti-twitter',
  'title'     => __( 'Social Links', 'dylan_addons' ),
  'desc'      => '',
  'fields'    => array(
    array(
      'id'      =>'facebook',
      'type'    => 'text',
      'title'   => __('Facebook', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'twitter',
      'type'    => 'text',
      'title'   => __('Twitter', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'google-plus',
      'type'    => 'text',
      'title'   => __('Google Plus', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'instagram',
      'type'    => 'text',
      'title'   => __('Instagram', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'linkedin',
      'type'    => 'text',
      'title'   => __('Linkedin', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'youtube',
      'type'    => 'text',
      'title'   => __('Youtube', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'pinterest',
      'type'    => 'text',
      'title'   => __('Pinterest', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'dribbble',
      'type'    => 'text',
      'title'   => __('Dribbble', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'tumblr',
      'type'    => 'text',
      'title'   => __('Tumblr', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'flickr',
      'type'    => 'text',
      'title'   => __('Flickr', 'dylan_addons'),
      'default' => '',
    ),
    array(
      'id'      =>'github',
      'type'    => 'text',
      'title'   => __('Github', 'dylan_addons'),
      'default' => '',
    ),
  )
);

/* Shop */
$this->sections[] = array(
  'icon'      => 'ti-shopping-cart',
  'title'     => __( 'Shop', 'dylan_addons' ),
  'fields'    => array(
    array(
      'id'    => 'shop_sidebar',
      'title' => __('Show Sidebar on Shop Page', 'dylan_addons'),
      'type'  => 'switch',
      'default' => true,
    ),
    array(
      'id' => 'shop_columns',
      'title' => __('Number of Columns', 'dylan_addons'),
      'type' => 'select',
      'options' => array(
        '2' => '2',
        '3' => '3',
        '4' => '4',
      ),
      'default' => '3'
    )
  )
);

/* 404 */
$this->sections[] = array(
  'icon'      => 'ti-alert',
  'title'     => __( '404 Page', 'dylan_addons' ),
  'fields'    => array(
    array(
      'id'  => 'error_bg_image',
      'title' => __( 'Background Image', 'dylan_addons' ),
      'type'  => 'media'
    ),
    array(
      'id'    => 'error_title',
      'title' => __('Error Page Title', 'dylan_addons'),
      'type'  => 'text',
      'default' => __('Error 404', 'dylan_addons')
    ),
    array(
      'id'    => 'error_text',
      'title' => __('Error Page Text', 'dylan_addons'),
      'type'  => 'textarea',
      'default' => __('The requested page was not found on this server. That’s all we know.', 'dylan_addons')
    ),
  )
);

/* Footer */
$this->sections[] = array(
  'icon'      => 'ti-layout-media-overlay-alt',
  'title'     => __( 'Footer', 'dylan_addons' ),
  'fields'    => array(
    array(
      'id'  => 'footer_text',
      'title' => __( 'Copyright Text', 'dylan_addons' ),
      'type'  => 'editor',
      'default' => '&copy; '.date('Y').' '. get_bloginfo('name').'. All rights reserved.',
      'args'    => array(
        'wpautop'       => false,
        'media_buttons' => false,
        'textarea_rows' => 5,
        'teeny'         => true,
        'quicktags'     => false,
      )
    )
  )
);

$alert = array(
  'id'    => 'info_import_again',
  'type'  => 'line',
  'style' => '',
  'desc'  => ''
);

if (get_option('dylan_demo_data_imported')) {
  $alert = array(
    'id'    => 'info_import_again',
    'type'  => 'info',
    'style' => 'critical',
    'desc'  => __("<b>Warning</b>: You have already imported the demo content. Import another one will create duplicate content. If you want to reimport demo content, delete existing content before proceed.", 'dylan_addons')
  );
}

/* Importer */
$this->sections[] = array(
  'icon'      => 'ti-settings',
  'title'     => __( 'Import Demo Data', 'dylan_addons' ),
  'desc'      => '',
  'fields'    => array(
    $alert,
    array(
      'id'    => 'info_import',
      'type'  => 'info',
      'style' => 'warning',
      'desc'  => __("<img style='height: 18px; margin-bottom: -5px; margin-right: 5px;' src='".get_template_directory_uri() . '/assets/images/loading.gif'."'> <span>Importing demo content... It may take a few minutes. Don't close the page!</span>", 'dylan_addons')
    ),
    array( 
      'id'       => 'dylan_import',
      'type'     => 'raw',
      'title'    => __('Import Demo', 'dylan_addons'),
      'desc'     => __('Click the button to make your website look exactly like the demo.', 'dylan_addons'),
      'content'  => '<button type="button" id="dylan-import-btn" class="button button-primary">Import</button>'
    ),
  )
);

/* Theme Support */
$this->sections[] = array(
  'icon'      => 'ti-support',
  'title'     => __( 'Theme Support', 'dylan_addons' ),
  'desc'      => '',
  'fields'    => array(
    array( 
      'id'       => 'dylan_support',
      'type'     => 'raw',
      'desc'     => __('Here you can read the theme documentation and watch video tutorials. If you do not find an answer to your problem, submit a ticket.', 'dylan_addons'),
      'content'  => '<a target="_blank" href="http://bit.ly/DylanDocs" class="button button-primary">Get Theme Support</a>',
      'full_width' => true
    ),
  )
);

?>
