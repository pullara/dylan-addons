<?php
/*
Plugin Name: Dylan Addons
Plugin URI: https://hody.co
Description: Awesome Addons for Dylan Theme.
Version: 1.0.5
Author: HodyLab
Author URI: https://hody.co
*/

define( 'DYLAN_ADDONS_PATH', plugin_dir_path(__FILE__) );

// don't load directly
if (!defined('ABSPATH')) die('-1');

class DylanExtendAddonClass {

  function __construct() {

    require_once(DYLAN_ADDONS_PATH . '/importer/importer.php');

    require_once(DYLAN_ADDONS_PATH . '/admin/admin.php');

    // We safely integrate with VC with this hook
    add_action( 'vc_before_init', array( $this, 'integrateWithVC' ) );

    add_action( 'vc_before_init', array( $this, 'dylanRows' ) );

    add_action( 'vc_before_init', array( $this, 'dylanTtaSections' ) );

    add_action( 'vc_before_init', array( $this, 'update_elements' ) );

    add_action( 'plugins_loaded', array( $this, 'init_languages') );

  }

  public function init_languages(){
    load_plugin_textdomain( 'dylan_addons', false, basename( dirname( __FILE__ ) ) . '/languages' );
  }

  public function integrateWithVC() {

    if ( ! defined( 'WPB_VC_VERSION' ) ) {
      add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
      return;
    }

    require_once(DYLAN_ADDONS_PATH . '/vc/vc_elements.php');
    require_once(DYLAN_ADDONS_PATH . '/vc/vc_shortcodes.php');
    require_once(DYLAN_ADDONS_PATH . '/vc/vc_icons.php');
    require_once(DYLAN_ADDONS_PATH . '/vc/vc_defaults.php');
    vc_add_shortcode_param( 'dropdown_multi', array($this, 'vc_dropdown_multi') );
    vc_add_shortcode_param( 'datepicker', array($this, 'vc_datepicker') );
    vc_add_shortcode_param( 'hidden_input', array($this, 'vc_hidden_input') );
    vc_add_shortcode_param( 'attach_video', array($this, 'vc_video') );

    vc_disable_frontend();
    vc_set_as_theme();

    // Hook for admin editor.
    add_action( 'vc_build_admin_page', array($this, 'remove_elements'), 11 );
    // Hook for frontend editor.
    add_action( 'vc_load_shortcode', array($this, 'remove_elements'), 11 );

  }

  function update_elements(){
    $wp_el = array('vc_wp_search', 'vc_wp_meta', 'vc_wp_recentcomments', 'vc_wp_calendar', 'vc_wp_pages', 'vc_wp_tagcloud', 'vc_wp_custommenu', 'vc_wp_text', 'vc_wp_posts', 'vc_wp_categories', 'vc_wp_archives', 'vc_wp_rss');

    foreach ($wp_el as $key => $value) {
      vc_map_update($value, array('icon' => 'ti-wordpress'));
    }

    $woo_el = array('woocommerce_cart', 'woocommerce_checkout', 'woocommerce_order_tracking', 'woocommerce_my_account', 'recent_products', 'featured_products', 'product', 'products', 'add_to_cart', 'add_to_cart_url', 'product_page', 'product_category', 'product_categories', 'sale_products', 'best_selling_products', 'top_rated_products', 'product_attribute');

    if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
      foreach ($woo_el as $key => $value) {
        vc_map_update($value, array('icon' => 'ti-shopping-cart'));
      }
    }

    $vc_el = array(
      'contact-form-7' => 'ti-email',
      'vc_row' => 'ti-layout-tab-window',
      'vc_section' => 'ti-layout-tab-window',
      'vc_column_text' => 'ti-text',
      'vc_separator' => 'ti-arrows-horizontal',
      'vc_text_separator' => 'ti-more',
      'vc_single_image' => 'ti-image',
      'vc_custom_heading' => 'ti-uppercase',
      'vc_widget_sidebar' => 'ti-layout-sidebar-left',
      'vc_video' => 'ti-video-camera',
      'vc_raw_html' => 'ti-html5',
      'vc_flickr' => 'ti-flickr',
      'vc_line_chart' => 'ti-bar-chart',
      'vc_empty_space' => 'ti-arrows-vertical',
      'vc_tta_pageable' => 'ti-layout-slider-alt',
      'vc_round_chart' => 'ti-control-record',
      'vc_zigzag' => 'ti-bolt-alt',
      'vc_hoverbox' => 'ti-widget',
    );

    foreach ($vc_el as $key => $value) {
      vc_map_update($key, array('icon' => $value));
    }
  }

  function remove_elements() {

    $elements = array( 'icon', 'masonry_media_grid', 'masonry_grid', 'basic_grid', 'media_grid', 'tta_accordion', 'tta_tabs', 'tta_tour', 'btn', 'toggle', 'cta', 'facebook', 'tweetmeme', 'googleplus', 'pinterest', 'tabs', 'tour', 'accordion', 'button', 'button2', 'cta_button', 'cta_button2', 'message', 'progress_bar', 'gmaps', 'posts_slider', 'image_carousel', 'raw_js', 'images_carousel', 'pie', 'gallery');

    foreach ( $elements as $key) {
      vc_remove_element( 'vc_'.$key );
    }

  }

  function vc_datepicker( $settings, $value ) {
    $value = htmlspecialchars( $value );

    return '<input name="' . $settings['param_name']
           . '" class="wpb_vc_param_value wpb-textinput '
           . $settings['param_name'] . ' ' . $settings['type']
           . '" type="date" value="' . $value . '"/>';
  }

  function vc_hidden_input( $settings, $value ) {
    $value = htmlspecialchars( $value );

    return '<input name="' . $settings['param_name']
           . '" class="wpb_vc_param_value wpb-textinput '
           . $settings['param_name'] . ' ' . $settings['type']
           . '" type="hidden" value="' . $value . '"/>';
  }

  function vc_video( $settings, $value ) {
    $value = htmlspecialchars( $value );

    return '<input name="' . $settings['param_name']
         . '" class="wpb_vc_param_value wpb-textinput '
         . $settings['param_name'] . ' ' . $settings['type']
         . '" type="text" value="' . $value . '" style="width: 75%;">
        <button style="height: 35px;" class="button upload_video_button" type="button">'.__('Browse Videos', 'dylan-wp') .'</button>';
  }

  function vc_dropdown_multi( $settings, $value ) {
    $output = '';
    $css_option = str_replace( '#', 'hash-', vc_get_dropdown_option( $settings, $value ) );
    $output .= '<select name="'
               . $settings['param_name']
               . '" multiple="true" class="wpb_vc_param_value wpb-input wpb-select '
               . $settings['param_name']
               . ' ' . $settings['type']
               . ' ' . $css_option
               . '" data-option="' . $css_option . '">';
    if ( is_array( $value ) ) {
      $value = isset( $value['value'] ) ? $value['value'] : array_shift( $value );
    }
    if ( ! empty( $settings['value'] ) ) {
      foreach ( $settings['value'] as $index => $data ) {
        if ( is_numeric( $index ) && ( is_string( $data ) || is_numeric( $data ) ) ) {
          $option_label = $data;
          $option_value = $data;
        } elseif ( is_numeric( $index ) && is_array( $data ) ) {
          $option_label = isset( $data['label'] ) ? $data['label'] : array_pop( $data );
          $option_value = isset( $data['value'] ) ? $data['value'] : array_pop( $data );
        } else {
          $option_value = $data;
          $option_label = $index;
        }
        $selected = '';
        $option_value_string = (string) $option_value;
        $current_value = strlen( $value ) > 0 ? explode( ',', $value ) : array();
        if ( count( $current_value ) > 0 && in_array( $option_value_string, $current_value ) ) {
          $selected = ' selected="selected"';
        }
        $option_class = str_replace( '#', 'hash-', $option_value );
        $output .= '<option class="' . esc_attr( $option_class ) . '" value="' . esc_attr( $option_value ) . '"' . $selected . '>'
                   . htmlspecialchars( $option_label ) . '</option>';
      }
    }
    $output .= '</select>';

    return $output;
  }

  /*
  Show notice if your plugin is activated but Visual Composer is not
  */
  public function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
    <div class="updated">
      <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
    </div>';
  }

  public function dylanTtaSections(){

    $params_to_remove = array('add_icon', 'i_type', 'i_position', 'i_icon_fontawesome', 'i_icon_openiconic', 'i_icon_typicons', 'i_icon_entypo', 'i_icon_linecons', 'i_icon_monosocial', 'el_class');

    foreach ($params_to_remove as $par) {
      vc_remove_param('vc_tta_section', $par);
    }

    $parent_tag = vc_post_param( 'parent_tag', '' );
    $include_icon_params = ( 'vc_tta_pageable' !== $parent_tag );

    if ($include_icon_params) {
      vc_add_params('vc_tta_section', array(
        array(
          'type' => 'iconpicker',
          'heading' => __('Icon', 'dylan-wp'),
          'param_name' => 'icon',
          'weight' => 10,
          'settings' => array(
            'type' => 'hodyicons',
            'emptyIcon' => true,
            'iconsPerPage' => 100
          )
        ),
        array(
          'type' => 'hidden_input',
          'param_name' => 'i_position',
          'value' => 'left'
        ),
      ));
    }

  }

  public function dylanRows(){

    vc_remove_param('vc_row', 'columns_placement');
    vc_remove_param('vc_row', 'gap');
    vc_remove_param('vc_row', 'equal_height');
    vc_remove_param('vc_row', 'video_bg_parallax');
    vc_remove_param('vc_row', 'video_bg_url');
    vc_remove_param('vc_row', 'el_id');
    vc_remove_param('vc_row', 'el_class');
    vc_remove_param('vc_row', 'parallax_speed_video');
    vc_remove_param('vc_row', 'parallax_speed_bg');
    vc_remove_param('vc_row', 'css_animation');

    vc_add_param('vc_column_text', array(
      'type' => 'dropdown',
      'value' => array(
        'Default'    => '',
        'Serif'  => 'serif',
        'Cursive'   => 'cursive'
      ),
      'heading' => __('Font Style', 'dylan-wp'),
      'param_name' => 'font_style',
      'std' => '',
    ));

    vc_add_params('vc_row', array(
      array(
        'type' => 'dropdown',
        'heading' => __( 'Use video background?', 'dylan-wp' ),
        'param_name' => 'video_bg',
        'description' => __( 'If checked, video will be used as row background.', 'dylan-wp' ),
        'value' => array(
          'No' => '',
          'Yes, from YouTube' => 'youtube',
          'Yes, self hosted video' => 'self_hosted'
        ),
      ),
      array(
        'type' => 'textfield',
        'heading' => __( 'YouTube link', 'dylan-wp' ),
        'param_name' => 'video_bg_url',
        'value' => 'https://www.youtube.com/watch?v=lMJXxhRFO1k',
        'description' => __( 'Add YouTube link.', 'dylan-wp' ),
        'dependency' => array(
          'element' => 'video_bg',
          'value' => 'youtube',
        ),
      ),
      array(
        'type' => 'attach_image',
        'value' => '',
        'heading' => __('Fallback Image for mobile devices. (YouTube videos don\'t work on mobile.)', 'dylan'),
        'param_name' => 'fallback_image',
        'dependency' => array(
          'element' => 'video_bg',
          'value' => 'youtube',
        ),
      ),
      array(
        'type' => 'attach_video',
        'heading' => __( 'Self Hosted Video', 'dylan-wp' ),
        'param_name' => 'self_hosted_video',
        'value' => '',
        'description' => __( 'Select or upload a video.', 'dylan-wp' ),
        'dependency' => array(
          'element' => 'video_bg',
          'value' => 'self_hosted',
        ),
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Parallax', 'dylan-wp' ),
        'param_name' => 'parallax',
        'value' => array(
          __( 'No', 'dylan-wp' ) => '',
          __( 'Yes', 'dylan-wp' ) => 'content-moving',
        ),
        'description' => __( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'dylan-wp' ),
        'dependency' => array(
          'element' => 'video_bg',
          'is_empty' => true,
        ),
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Dark'    => '',
          'Extra Dark'    => 'extra-dark',
          'Light'  => 'light',
          'Colored' => 'colored',
          'Gradient' => 'gradient',
        ),
        'heading' => __('Overlay Style', 'dylan-wp'),
        'param_name' => 'overlay_style',
        'std' => '',
        'dependency' => array(
          'element' => 'parallax',
          'not_empty' => true,
        ),
      ),
      array(
        'type' => 'checkbox',
        'heading' => __( 'Full height row?', 'dylan-wp' ),
        'param_name' => 'full_height',
        'description' => __( 'If checked row will be set to full height.', 'dylan-wp' ),
        'value' => array( __( 'Yes', 'dylan-wp' ) => 'yes' ),
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Content position', 'dylan-wp' ),
        'param_name' => 'content_placement',
        'value' => array(
          __( 'Default', 'dylan-wp' ) => '',
          __( 'Middle', 'dylan-wp' ) => 'middle',
        ),
        'description' => __( 'Select content position within columns.', 'dylan-wp' ),
      ),
      array(
        'type' => 'el_id',
        'heading' => __( 'Row ID', 'dylan-wp' ),
        'param_name' => 'el_id',
        'description' => sprintf( __( 'Enter row ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'dylan-wp' ), 'http://www.w3schools.com/tags/att_global_id.asp' ),
      ),
      array(
        'type' => 'textfield',
        'heading' => __( 'Extra class name', 'dylan-wp' ),
        'param_name' => 'el_class',
        'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'dylan-wp' ),
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Row Background Color', 'dylan-wp'),
        'param_name' => 'row_bg_color',
        'value' => array(
          'White' => '',
          'Grey' => 'grey-bg',
          'Dark' => 'dark-bg',
          'Colored' => 'colored-bg',
        ),
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'std' => ''
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Split Row?', 'dylan-wp'),
        'param_name' => 'split_row',
        'value' => array( 'No' => '', 'Yes' => 'yes' ),
        'description' => __("Use as split row with background image or a map.", "dylan"),
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'std' => ''
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Split Row Background', 'dylan-wp'),
        'param_name' => 'split_row_bg_type',
        'value' => array( 'Image' => 'image', 'Map' => 'map' ),
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'split_row',
          'value' => array('yes')
        ),
        'std' => ''
      ),
      array(
        'type' => 'attach_image',
        'value' => '',
        'heading' => __('Split Row Background Image', 'dylan-wp'),
        'param_name' => 'split_row_bg',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('image')
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Background Image Position', 'dylan-wp'),
        'param_name' => 'split_bg_position',
        'value' => array('Left' => 'left', 'Right' => 'right'),
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('image')
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Split Row Width', 'dylan-wp'),
        'param_name' => 'split_row_width',
        'group' => __( 'Dylan Options', 'dylan-wp'),
        'value' => array(
          '1 column - 1/12' => 'vc_col-md-1',
          '2 columns - 1/6' => 'vc_col-md-2',
          '3 columns - 1/4' => 'vc_col-md-3',
          '4 columns - 1/3' => 'vc_col-md-4',
          '5 columns - 5/12' => 'vc_col-md-5',
          '6 columns - 1/2' => 'vc_col-md-6',
          '7 columns - 7/12' => 'vc_col-md-7',
          '8 columns - 2/3' => 'vc_col-md-8',
          '9 columns - 3/4' => 'vc_col-md-9',
          '10 columns - 5/6' => 'vc_col-md-10',
          '11 columns - 11/12' => 'vc_col-md-11',
          '12 columns - 1/1' => 'vc_col-md-12',
        ),
        'std' => 'vc_col-md-6',
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('image')
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Split Row Width (Tablet)', 'dylan-wp'),
        'param_name' => 'split_row_width_sm',
        'group' => __( 'Dylan Options', 'dylan-wp'),
        'value' => array(
          '1 column - 1/12' => 'vc_col-sm-1',
          '2 columns - 1/6' => 'vc_col-sm-2',
          '3 columns - 1/4' => 'vc_col-sm-3',
          '4 columns - 1/3' => 'vc_col-sm-4',
          '5 columns - 5/12' => 'vc_col-sm-5',
          '6 columns - 1/2' => 'vc_col-sm-6',
          '7 columns - 7/12' => 'vc_col-sm-7',
          '8 columns - 2/3' => 'vc_col-sm-8',
          '9 columns - 3/4' => 'vc_col-sm-9',
          '10 columns - 5/6' => 'vc_col-sm-10',
          '11 columns - 11/12' => 'vc_col-sm-11',
          '12 columns - 1/1' => 'vc_col-sm-12',
        ),
        'std' => 'vc_col-sm-4',
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('image')
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Show Title?', 'dylan-wp'),
        'param_name' => 'show_title',
        'value' => array( 'No' => '', 'Yes' => 'yes' ),
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'std' => '',
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('image')
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Section Title', 'dylan-wp'),
        'param_name' => 'split_title',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Section Subtitle', 'dylan-wp'),
        'param_name' => 'split_subtitle',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Dark'    => '',
          'Light'  => 'light',
        ),
        'heading' => 'Text Color',
        'param_name' => 'split_text_color',
        'std' => '',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => 'Text Align',
        'param_name' => 'split_text_align',
        'std' => 'center',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          '' => '',
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => 'Text Align (Tablet)',
        'param_name' => 'split_text_align_sm',
        'std' => '',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          '' => '',
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => 'Text Align (Smartphone)',
        'param_name' => 'split_text_align_xs',
        'std' => '',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'None' => '',
          'Uppercase'   => 'upper',
        ),
        'heading' => 'Text Transform',
        'param_name' => 'split_text_transform',
        'std' => '',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Additional Content', 'dylan-wp'),
        'param_name' => 'additional_content',
        'description' => __('You can add an overlay here. Example: [dylan_overlay][dylan_play_button url="IFRAME_URL_HERE"][/dylan_overlay] ', 'dylan-wp'),
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'dependency' => array(
          'element' => 'show_title',
          'value' => array('')
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '40.773328',
        'heading' => __('Map Latitude', 'dylan-wp'),
        'param_name' => 'split_map_lat',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'description' => __('Find your Latitude and Longitude <a target="blank" href="http://www.latlong.net/">here</a>', 'dylan-wp'),
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('map')
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '-73.960088',
        'heading' => __('Map Longitude', 'dylan-wp'),
        'param_name' => 'split_map_lng',
        "group" => __( "Dylan Options", 'dylan-wp' ),
        'description' => __('Find your Latitude and Longitude <a target="blank" href="http://www.latlong.net/">here</a>', 'dylan-wp'),
        'dependency' => array(
          'element' => 'split_row_bg_type',
          'value' => array('map')
        )
      ),
    ));
  }
}

new DylanExtendAddonClass();

/* Register Portfolio Post Type */
function dylan_portfolio_init(){
  register_post_type(
    'portfolio',
    array(
      'labels' => array(
        'name'          => 'Portfolio',
        'singular_name' => 'Portfolio'
      ),
      'public'      => true,
      'has_archive' => true,
      'supports'    => array('title','thumbnail','editor'),
      'show_in_nav_menus'   => true,
    )
  );

  register_taxonomy(
    'portfolio_category',
    'portfolio',
    array(
      'hierarchical' => true,
      'label'        => 'Categories',
      'query_var'    => true,
      'rewrite'      => true
    )
  );
}

add_action('init', 'dylan_portfolio_init');
