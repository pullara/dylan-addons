<?php   

add_action('wp_ajax_dylan_import_data', 'dylan_import_data');

if (!function_exists('dylan_import_data')) {

  @ini_set('max_execution_time', 600);
  
  function dylan_import_data($file){

    update_option('dylan_demo_data_imported', '');
    
    if ( ! class_exists('WP_Import') ) {
      require_once('wordpress-importer/wordpress-importer.php');
    }

    // Import Content
    $dylan_import = new WP_Import();
    $demo_file = get_template_directory() . '/framework/admin/demo-data.xml';

    if (file_exists($demo_file)) {
      $dylan_import->fetch_attachments = false; ob_start();
      $dylan_import->import($demo_file); ob_end_clean();    
    }

    // Update Reading Settings
    $onepage  = get_page_by_title('Classic Agency');
    $blogpage = get_page_by_title('Blog');
    if( isset($onepage->ID) && isset($blogpage->ID) ) {
      update_option('show_on_front', 'page');
      update_option('page_on_front',  $onepage->ID); 
      update_option('page_for_posts', $blogpage->ID); 
    }

    // Update Postmeta
    global $wpdb;
    $from_url = 'http://themes.hody.co/dylan';
    $to_url = home_url();
    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->postmeta} SET meta_value = REPLACE(meta_value, %s, %s)", $from_url, $to_url));
    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = REPLACE(post_content, %s, %s)", 'http%3A%2F%2Fthemes.hody.co%2Fdylan%2Fblog%2F', '%23'));

    $menus = wp_get_nav_menu_items('Primary');
    if (is_array($menus)) {
      foreach ($menus as $menu_item) {
        if ($menu_item->url == '#' && in_array($menu_item->post_name, array('home', 'elements'))) {
          update_post_meta( $menu_item->ID, '_dylan_mega_menu', 1 );
          $mens[] = $menu_item->ID;
        } elseif ($menu_item->url == '#' && in_array($menu_item->post_name, array('layouts', 'layouts-2', 'layouts-3', 'layouts-4', 'home-layouts', 'home-layouts-2', 'home-layouts-3', 'home-layouts-4', 'elements-2', 'elements-3', 'elements-4'))) {
          update_post_meta( $menu_item->ID, '_dylan_menu_label', 1 );
          $mens[] = $menu_item->ID;
        }
      }
    }


    // Update navigation menus
    $default_menu = get_term_by('name', 'Primary', 'nav_menu');
    $locations = get_theme_mod('nav_menu_locations');
    $locations['primary'] = $default_menu->term_id;
    set_theme_mod('nav_menu_locations', $locations);

    $onepage_menus = array('App Landing', 'Architecture', 'Construction', 'Event', 'Gym', 'Music', 'Restaurant', 'Resume', 'SPA');

    foreach ($onepage_menus as $pag) {
      $page_name = get_page_by_title($pag);
      $menu_id = get_term_by('name', $pag, 'nav_menu');
      update_post_meta($page_name->ID, 'dylan_menu_id', $menu_id->term_id);
    }

    update_option('dylan_demo_data_imported', '1');
    die();
    
  }
}

add_action('wp_ajax_dylan_import_attachments', 'dylan_import_attachments');

if (!function_exists('dylan_import_attachments')) {
  
  function dylan_import_attachments($file){

    update_option('dylan_demo_attachments_imported', '');

    if ( ! class_exists('WP_Import') ) {
      require_once('wordpress-importer/wordpress-importer.php');
    }

    // Import Content
    $dylan_import_attachments = new WP_Import();
    $demo_attachments = get_template_directory() . '/framework/admin/demo-attachments.xml';

    if (file_exists($demo_attachments)) {
      $dylan_import_attachments->fetch_attachments = true; ob_start();
      $dylan_import_attachments->import($demo_attachments); ob_end_clean();    
    }

    update_option('dylan_demo_attachments_imported', '1');
    die();

  }
}

add_action('wp_ajax_dylan_check_import', 'dylan_check_import');

if (!function_exists('dylan_check_import')) {

  function dylan_check_import(){
    $imported_posts = (get_option('dylan_demo_data_imported') == '1') ? 1 : 0;
    $imported_images = (get_option('dylan_demo_attachments_imported') == '1') ? 1 : 0;

    $arr = array(
      'posts' => $imported_posts,
      'images' => $imported_images
    );

    echo json_encode($arr, JSON_PRETTY_PRINT);
    die();
  }

}

?>
