<?php

require_once('shortcodes/home_slider.php');
require_once('shortcodes/text_rotator.php');
require_once('shortcodes/page_title.php');
require_once('shortcodes/services.php');
require_once('shortcodes/icon_box.php');
require_once('shortcodes/clients.php');
require_once('shortcodes/testimonials.php');
require_once('shortcodes/blog_summary.php');
require_once('shortcodes/button.php');
require_once('shortcodes/portfolio.php');
require_once('shortcodes/tabs.php');
require_once('shortcodes/progress_bars.php');
require_once('shortcodes/team_member.php');
require_once('shortcodes/carousel.php');
require_once('shortcodes/pricing_table.php');
require_once('shortcodes/photo_gallery.php');
require_once('shortcodes/menu.php');
require_once('shortcodes/counter.php');
require_once('shortcodes/social_icons.php');
require_once('shortcodes/timeline.php');
require_once('shortcodes/common.php');
require_once('shortcodes/accordion.php');
require_once('shortcodes/image_slider.php');

?>
