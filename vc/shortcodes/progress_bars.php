<?php

add_shortcode( 'dylan_progress_bar', 'dylan_progress_bar' );

function dylan_progress_bar( $atts ) {
  extract( shortcode_atts( array(
    'bg_color' => 'black',
    'style' => '',
    'name'  => '',
    'icon'  => '',
    'percentage' => ''
  ), $atts ) );

  $output = '';

  switch ($style) {
    case 'circle':
      $add_class = ($icon) ? 'no-text' : '';
      $circle_color = '#222225';
      if ($bg_color == 'colored') {
        $circle_color = dylan_options('primary_color');
      } elseif ($bg_color == 'white') {
        $circle_color = '#fff';
      }
      
      $output .= '<div class="circle-bar '.$bg_color.'">';
      $output .= '<div class="circle-bar-wrap '.$add_class.'" data-color="'.$circle_color.'" data-number="'.esc_attr($percentage).'">';
      if ($icon) {
        $output .= '<i class="'.$icon.'"></i>';
      }
      $output .= '</div>';
      $output .= '<h5 class="bar-label">'.esc_attr($name).'</h5>';
      $output .= '</div>';
      break;
    
    default:
      $output .= '<div class="skill">';
      $output .= '<h5 class="skill-name">'.esc_attr($name);
      $output .= '<span class="skill-perc">'.esc_attr($percentage).'%</span></h5>';
      $output .= '<div class="progress">';
      $output .= '<div role="progressbar" class="progress-bar '.$bg_color.'" data-progress="'.esc_attr($percentage).'"></div>';
      $output .= '</div>';
      $output .= '</div>';
      break;
  }

  return $output;
}
