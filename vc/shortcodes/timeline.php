<?php

add_shortcode( 'dylan_timeline', 'dylan_timeline' );

function dylan_timeline( $atts ) {
  extract( shortcode_atts( array(
    'items' => ''
  ), $atts ) );

  $timeline_items = vc_param_group_parse_atts($items);

  $output = '<ul class="timeline">';
  if ($timeline_items) {
    foreach ($timeline_items as $item) {
      $output .= '<li>';
      
      $output .= '<div class="timeline-content">';
      if (isset($item['title'])) {
        $output .= '<h4>'.esc_attr($item['title']).'</h4>';
      }
      $output .= '<span>';
      if (isset($item['place'])) {
        $output .= '<b>'.esc_attr($item['place']). '</b>';
      }
      if (isset($item['date'])) {
        $output .= esc_attr($item['date']);
      }
      $output .= '</span>';
      if (isset($item['text'])) {
        $output .= '<p>'.esc_attr($item['text']).'</p>';
      }
      $output .= '</div>';

      $output .= '</li>';
    }
  }
  $output .= '</ul>';

  return $output;

}
