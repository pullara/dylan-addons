<?php

add_shortcode('dylan_team_member', 'dylan_team_member');

function dylan_team_member($atts){
  extract( shortcode_atts( array(
    'style' => '',
    'thumb_size' => '',
    'image' => '',
    'name' => '',
    'role' => '',
    'description' => '',
    'socials' => ''
  ), $atts ) );

  $links = vc_param_group_parse_atts($socials);

  $icon_classes = array(
    'facebook' => 'hc-facebook',
    'twitter' => 'hc-twitter-alt',
    'linkedin' => 'hc-linkedin',
    'instagram' => 'hc-instagram',
    'dribbble' => 'hc-dribbble',
    'github' => 'hc-github',
    'flickr' => 'hc-flickr',
    'pinterest' => 'hc-pinterest',
    'youtube' => 'hc-youtube',
    'tumblr' => 'hc-tumblr-alt',
  );

  $output = '';

  switch ($style) {
    case 'overlay':
      $output = '<div class="member-image">';
      $image_src = wp_get_attachment_image_src($image, 'dylan_small');
      $output .= '<img src="'.esc_url($image_src[0]).'" alt="'.esc_attr($name).'">';
      $output .= '<div class="member-overlay">';
      $output .= '<h3>'.esc_attr($name).'</h3>';
      $output .= '<span>'.esc_attr($role).'</span>';
      if ($socials) {
        $output .= '<ul class="team-social">';
        foreach ($links as $link) {
          $output .= '<li>';
          $output .= '<a href="'.esc_url($link['url']).'" target="_blank">';
          $output .= '<i class="'.$icon_classes[$link['social']].'"></i>';
          $output .= '</a>';
          $output .= '</li>';
        }
        $output .= '</ul>';
      }      
      $output .= '</div>';
      $output .= '</div>';
      break;
    
    default:
      $output = '<div class="team-member '.$style.' '.$thumb_size.'">';
      if ($image) {
        $output .= '<div class="team-image">';
        $img_size = ($style == 'round-thumb') ? array(300,300, true) : 'dylan_small';
        $image_src = wp_get_attachment_image_src($image, $img_size);
        $output .= '<img src="'.esc_url($image_src[0]).'" alt="'.esc_attr($name).'">';
        if ($style != 'round-thumb') {
          $output .= '<div class="team-overlay">';
          if ($socials) {
            $output .= '<ul class="team-social">';
            foreach ($links as $link) {
              if (isset($link['url'])) {
                $output .= '<li>';
                $output .= '<a href="'.esc_url($link['url']).'" target="_blank">';
                $output .= '<i class="'.$icon_classes[$link['social']].'"></i>';
                $output .= '</a>';
                $output .= '</li>';
              }
            }
            $output .= '</ul>';
          }
          $output .= '</div>';
        }
        $output .= '</div>';
      }
      $output .= '<div class="team-info">';
      $output .= '<h4>'.esc_attr($name).'</h4>';
      $output .= '<span class="serif">'.esc_attr($role).'</span>';
      $output .= '<p>'.esc_attr($description).'</p>';
      if ($style == 'round-thumb') {
        $output .= '<ul class="team-social">';
        foreach ($links as $link) {
          if (isset($link['url'])) {
            $output .= '<li class="social-item-'.$link['social'].'">';
            $output .= '<a href="'.esc_url($link['url']).'" target="_blank">';
            $output .= '<i class="'.$icon_classes[$link['social']].'"></i>';
            $output .= '</a>';
            $output .= '</li>';
          }
        }
        $output .= '</ul>';
      }
      $output .= '</div>';
      $output .= '</div>';
      break;
  }

  return $output;

}
