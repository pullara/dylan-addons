<?php

add_shortcode('dylan_counter', 'dylan_counter');

function dylan_counter($atts){
  extract( shortcode_atts( array(    
    'text' => '',
    'number' => '',
    'icon'  => '',
    'icon_position' => '',
    'icon_color' => '',
    'font_style' => ''
  ), $atts ) );

  $output = '<div class="counter '.$icon_position.'">';
  if ($icon) {
    $output .= '<div class="counter-icon '.$icon_color.'">';
    $output .= '<i class="'.$icon.'"></i>';
    $output .= '</div>';
    $output .= '<div class="counter-content">';
  }
  $output .= '<h5 class="'.$font_style.'"><span class="number-count" data-count="'.esc_attr($number).'">'.esc_attr($number).'</span></h5>';
  $output .= '<span>'.esc_attr($text).'</span>';
  if ($icon) {
    $output .= '</div>';
  }
  $output .= '</div>';

  return $output;

}
