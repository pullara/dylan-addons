<?php

add_shortcode('dylan_social_icons', 'dylan_social_icons');

function dylan_social_icons($atts){
  extract( shortcode_atts( array(
    'socials' => '',
    'style' => '',
    'icons_align' => 'center'
  ), $atts ) );  

  $links = vc_param_group_parse_atts($socials);  

  $icon_classes = array(
    'facebook' => 'hc-facebook',
    'twitter' => 'hc-twitter-alt',
    'linkedin' => 'hc-linkedin',
    'instagram' => 'hc-instagram',
    'dribbble' => 'hc-dribbble',
    'github' => 'hc-github',
    'flickr' => 'hc-flickr',
    'pinterest' => 'hc-pinterest',
    'youtube' => 'hc-youtube',
    'tumblr' => 'hc-tumblr-alt',
  );
  
  $output = '<ul class="social-list text-'.$icons_align.' '.$style.'">';
  foreach ($links as $link) {
    $output .= '<li class="social-item-'.$link['social'].'">';
    $output .= '<a href="'.esc_url($link['url']).'" target="_blank">';
    $output .= '<i class="'.$icon_classes[$link['social']].'"></i>';
    $output .= '</a>';
    $output .= '</li>';
  }
  $output .= '</ul>';

  return $output;

}
