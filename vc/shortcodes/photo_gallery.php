<?php

add_shortcode( 'dylan_photo_gallery', 'dylan_photo_gallery' );

function dylan_photo_gallery( $atts ) {
  extract( shortcode_atts( array(
    'layout' => '',
    'photos' => ''
  ), $atts ) );

  $galleries = vc_param_group_parse_atts($photos);

  $output = '<ul class="photo-gallery '.$layout.'">';
  if ($galleries) {
    foreach ($galleries as $gallery) {

      $css_class = (isset($gallery['size']) && $gallery['size'] == 'half') ? ' class="half"': '';
      $image_src = wp_get_attachment_image_src($gallery['pic'], 'dylan_square_thumb');

      $output .= '<li '.$css_class.'>';
      $output .= '<figure>';

      $output .= '<div class="gallery-item">';
      
      $output .= '<a href="'.esc_url(wp_get_attachment_url($gallery['pic'])).'">';
      $output .= '<img src="'.esc_url($image_src[0]).'" alt="">';

      $output .= '<figcaption>';
      $output .= '<div class="centrize">';
      $output .= '<div class="v-center">';
      $output .= '<i class="hc-search"></i>';
      if (!empty($gallery['caption'])) {
        $output .= '<h4>'.esc_attr($gallery['caption']).'</h4>';
      }
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</figcaption>';
      $output .= '</a>';
      
      $output .= '</div>';

      $output .= '</figure>';
      $output .= '</li>';

    }
  }
  $output .= '</ul>';

  return $output;

}
