<?php

add_shortcode( 'dylan_menu', 'dylan_menu' );

function dylan_menu( $atts ) {
  extract( shortcode_atts( array(
    'style' => '',
    'font_style' => '',
    'font_variant' => '',
    'menu_items' => '',
  ), $atts ) );

  $items = vc_param_group_parse_atts($menu_items);

  $output = '';

  switch ($style) {
    case 'boxes':
      $output .= '<div class="dylan-menu-alt">';
      $output .= '<div class="row">';

      $cnt = 0;

      foreach ($items as $item) {
        $output .= '<div class="col-md-6">';
        if ($cnt == 4) {
          $cnt = 0;
        }
        $cnt++;
        
        $item_class = 'dy-menu ';
        if ($cnt == 3 || $cnt == 4) {
          $item_class .= 'image-right';
        }

        $output .= '<div class="'.$item_class.'">';

        $menu_link = isset($item['link']) ? vc_build_link($item['link']) : array();
        $link_target = (!empty($menu_link['target'])) ? 'target="'.$menu_link['target'].'"' : '';

        if (isset($item['add_link']) && $item['add_link'] == 'yes' && isset($menu_link['url'])) {
          $output .= '<a '.$link_target.' href="'.esc_url($menu_link['url']).'">';
        }
        
        $output .= '<div class="dy-menu-media">';
        if (isset($item['image'])) {
          $image = wp_get_attachment_image_src($item['image'], 'dylan_small');
          $output .= '<img src="'.esc_url($image[0]).'" alt="" class="thumb-placeholder">';
        }
        $output .= '</div>';

        $output .= '<div class="dy-menu-body">';
        if (isset($item['title'])) {
          $output .= '<h2>'.esc_attr($item['title']).'</h2>';
        }
        if (isset($item['text'])) {
          $output .= '<p>'.esc_attr($item['text']).'</p>';
        }
        if (isset($item['infoline'])) {
          $output .= '<h4>'.esc_attr($item['infoline']).'</h4>';
        }
        $output .= '</div>';

        if (isset($item['add_link']) && $item['add_link'] == 'yes' && isset($menu_link['url'])) {
          $output .= '</a>';
        }

        $output .= '</div>';
        $output .= '</div>';
      }

      $output .= '</div>';
      $output .= '</div>';
      break;
    
    default:
      $output .= '<ul class="'.trim('dylan-menu ').'">';

      foreach ($items as $item) {
        $output .= (isset($item['text']) && $item['text'] != '' ) ? '<li class="menu-item-with-text '.$font_style.'">' : '<li class="'.$font_style.'">';

        if ( isset($item['add_image']) && $item['add_image'] == 'yes' && $item['image'] != '') {
          $image = wp_get_attachment_image_src($item['image'], 'thumbnail');
          
          $output .= '<div class="dylan-menu-pic">';
          $output .= '<img src="'.$image[0].'" alt="'.$item['title'].'">';
          $output .= '</div>';
          $output .= '<div class="dylan-menu-content">';
        }

        $output .= '<h4 class="'.$font_variant.'"><span class="dylan-menu-title">';
        $output .= esc_attr($item['title']);
        if (isset($item['infoline'])) {
          $output .= '<span class="dylan-menu-value">'.$item['infoline'].'</span>';
        }
        $output .= '</span></h4>';

        if (isset($item['text']) && $item['text'] != '' ) {
          $output .= '<p class="dylan-menu-text">'.esc_attr($item['text']).'</p>';
        }

        if ( isset($item['add_image']) && $item['add_image'] == 'yes' && $item['image'] != '') {
          $output .= '</div>';
        }

        $output .= '</li>';
      }

      $output .= '</ul>';
      break;
  }

  return $output;

}
