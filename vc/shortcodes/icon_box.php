<?php

add_shortcode( 'dylan_icon_box', 'dylan_icon_box' );

function dylan_icon_box( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'number' => '',
    'title' => '',
    'text' => '',
    'box_style' => 'default',
    'icon'  => '',
    'text_box_style' => '',
    'small_style' => '',
    'icon_color' => '',
    'icon_position' => '',
    'alignment' => '',
    'icon_background' => 'yes',
  ), $atts ) );

  $icon_clr = (!empty($icon_color)) ? 'style="color: '.$icon_color.'"': '';

  $output = '';

  switch ($box_style) {
    case 'text_box':
      $output = '<div class="'.trim('text-box' . ' ' . $text_box_style).'">';
      $output .= '<h4>' . esc_attr($title) . '</h4>';
      $output .= '<div class="text-box-content"><p>'. wpb_js_remove_wpautop($content) . '</p></div>';
      $output .= '</div>';
      break;

    case 'number_box':
      $output = '<div class="number-box">';
      $output .= '<div class="number-box-holder">';
      $output .= '<h2>' . esc_attr($number) . '</h2>';
      $output .= '</div>';
      $output .= '<h4>' . esc_attr($title) . '</h4>';
      $output .= '<p>' . esc_attr($text) . '</p>';
      $output .= '</div>';
      break;
    
    case 'basic':
      $additional_class = (!$text) ? 'only-title': '';
      $additional_class .= ' '.$icon_position;
      $output .= '<div class="icon-box-basic '.$additional_class.'">';
      $output .= '<i class="'.$icon.'" '.$icon_clr.'></i>';
      $output .= '<h4>' . esc_attr($title) . '</h4>';
      if ($text) {
        $output .= '<p>' . wpb_js_remove_wpautop($text) . '</p>';
      }
      $output .= '</div>';
      break;
    case 'simple':
      $output .= '<div class="icon-box-simple">';
      $output .= '<i class="'.$icon.' mb-15"></i>';
      $output .= '<span>' . esc_attr($title) . '</span>';
      $output .= '</div>';
      break;
    case 'circular':
      $output .= '<div class="icon-box-circular">';
      $output .= '<div class="ib-icon">';
      $output .= '<i class="'.$icon.' mb-15"></i>';
      $output .= '</div>';
      $output .= '<div class="ib-content">';
      if ($title) {
        $output .= '<h4>' . esc_attr($title) . '</h4>';
      }
      $output .= '<p>' . esc_attr($text) . '</p>';
      $output .= '</div>';
      $output .= '</div>';
      break;
    default:
      $output .= '<div class="icon-box '.$alignment.' '.str_replace(',', ' ', $small_style).'">';
      if ($icon_background == 'yes') {
        $output .= '<div class="ib-icon">';
        $output .= '<i class="'.$icon.'"></i>';
      } else{
        $output .= '<i class="'.$icon.'" '.$icon_clr.'></i>';
      }
      if ($icon_background == 'yes') {
        $output .= '</div>';
      }
      $output .= '<div class="ib-content">';
      $output .= '<h4>' . esc_attr($title) . '</h4>';
      $output .= '<p>' . wpb_js_remove_wpautop($text) . '</p>';
      $output .= '</div>';
      $output .= '</div>';
      break;
  }

  return $output;

}
