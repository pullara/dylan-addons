<?php

add_shortcode('dylan_pricing_table', 'dylan_pricing_table');

function dylan_pricing_table($atts){
  extract( shortcode_atts( array(
    'show_icon' => '',
    'icon'  => '',
    'alignment' => '',
    'title' => '',
    'price' => '',
    'currency' => '$',
    'style' => '',
    'features' => '',
    'button' => '',
    'button_color' => 'color'
  ), $atts ) );

  $lines = vc_param_group_parse_atts($features);

  $output = '<div class="pricing-table '.$style.' '.$alignment.'">';
  
  $output .= '<div class="pricing-head">';
  if ($show_icon == 'yes') {
    $output .= '<i class="'.esc_attr($icon).'"></i>';
  }
  $output .= '<h4 class="upper">'.esc_attr($title).'</h4>';
  $output .= '</div>';
  
  $output .= '<div class="price">';
  $output .= '<h2>';
  $output .= '<span>'.esc_attr($currency).'</span>';
  $output .= esc_attr($price);
  $output .= '</h2>';
  $output .= '</div>';

  if ($lines) {
    $output .= '<ul class="features nav">';
    foreach ($lines as $line) {
      if (isset($line['text'])) {
        $output .= '<li><span>'.$line['text'].'</span></li>';
      }
    }
    $output .= '</ul>';
  }

  if ($button){
    $link = vc_build_link($button);
    $target = (!empty($link['target'])) ? 'target="'.$link['target'].'"' : '';

    $output .= '<div class="pricing-footer">';    
    $output .= '<a class="btn btn-'.$button_color.'" '.$target.' href="'.esc_url($link['url']).'">'.esc_attr($link['title']).'</a>';
    $output .= '</div>';
  }

  $output .= '</div>';
  
  return $output;
}
