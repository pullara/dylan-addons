<?php

add_shortcode( 'dylan_accordion', 'dylan_accordion' );

function dylan_accordion( $atts ) {
  extract( shortcode_atts( array(
    'behavior' => '',
    'items' => '',
    'open_first' => 'yes'
  ), $atts ) );

  $accordions = vc_param_group_parse_atts($items);
  $multiple = (isset($behavior) && $behavior == 'multiple') ? 'data-multiple="true"' : '' ;
  $open = ($open_first == 'yes') ? 'data-open-first="true"' : '' ;

  $output = '<ul '.$multiple.' '.$open.' class="accordion nav">';
  if ($accordions) {
    foreach ($accordions as $accordion) {

      $output .= '<li>';
      $output .= '<div class="accordion-title">';
      $output .= '<h4>'.esc_attr($accordion['title']).'</h4>';
      $output .= '</div>'; 
      $output .= '<div class="accordion-content">';
      $output .= '<p>'.esc_attr($accordion['text']).'</p>';
      $output .= '</div>';      
      $output .= '</li>';

    }
  }
  $output .= '</ul>';

  return $output;

}
