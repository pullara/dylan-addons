<?php

add_shortcode( 'dylan_image_slider', 'dylan_image_slider' );

function dylan_image_slider( $atts ) {
  extract( shortcode_atts( array(
    'animation' => 'fade',
    'control_nav' => '',
    'direction_nav' => '',
    'images' => ''
  ), $atts ) );

  $pics = vc_param_group_parse_atts($images);
  $opts = array('"animation": "'.$animation.'"');
  
  if ($control_nav != 'off') {
    $opts[] = '"controlNav": true';
  }
  if ($direction_nav != 'off') {
    $opts[] = '"directionNav": true';
  }

  $output = '<div class="flexslider image-slider" data-options="{'.htmlentities(implode(', ', $opts)).'}">';
  $output .= '<ul class="slides">';
  if ($pics) {
    foreach ($pics as $pic) {

      $show_caption = ( isset($pic['title']) && $pic['title'] != '' || isset($pic['text']) && $pic['text'] != '' );
      $link = (isset($pic['link'])) ? vc_build_link($pic['link']) : '';

      $output .= '<li>';
      $output .= '<img src="'.esc_url(wp_get_attachment_url($pic['pic'])).'" alt="">';
      if ($show_caption) {
        $output .= '<div class="slide-caption">';
        if (isset($pic['title']) && $pic['title'] != '') {
          $output .= '<div class="title"><h2>'.esc_attr($pic['title']).'</h2></div>';
        }
        if (isset($pic['text']) && $pic['text'] != '') {
          $output .= '<p class="mt-25 mb-25">'.esc_attr($pic['text']).'</p>';
        }
        if ($link['url']) {
          $output .= '<a class="upper small-link" href="'.esc_url($link['url']).'"><span>'.esc_attr($link['title']).'</span><i class="hc-arrow-right"></i></a>';
        }
        $output .= '</div>';
      }
      $output .= '</li>';

    }
  }
  $output .= '</ul>';
  $output .= '</div>';

  return $output;

}
