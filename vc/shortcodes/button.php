<?php

add_shortcode( 'dylan_button', 'dylan_button' );

function dylan_button( $atts ) {
  extract( shortcode_atts( array(
    'text'  => '',
    'link'  => '',
    'style' => 'color',
    'alignment' => 'inline-btn-container',
    'shape' => '',
    'size'  => '',
    'show_icon' => '',
    'icon'  => '',
    'custom_color' => '',
    'button_image' => '',
  ), $atts ) );

  $btn_link = vc_build_link($link);

  $btn_class =  array(
    'btn',
    'btn-'.$style,
    $shape,
  );

  if ($size != 'normal') {
    $btn_class[] = $size;
  }

  $btn_target = (!empty($btn_link['target'])) ? 'target="'.$btn_link['target'].'"' : '';
  $btn_custom_color = ($style == 'custom' && $custom_color != '') ? 'style="border-color: '.$custom_color.'; background-color: '.$custom_color.';"' : '';

  $output =  '<div class="btn-container '.$alignment.'">';
  
  if (isset($btn_link['url'])) {
    $output .= '<a href="'.esc_url($btn_link['url']).'" class="'.trim(implode(' ', $btn_class)).'" '.$btn_target.' '.$btn_custom_color.'>';

    if ($style == 'image' && $button_image) {
      $output .= '<img src="'.wp_get_attachment_url($button_image).'">';
    } else{
      $output .= esc_attr($text);
    
      if ($show_icon == 'yes') {
        $btn_animation = ($icon == 'hc-angle-right' || $icon == 'hc-arrow-right') ? 'btn-icon-animated' : '';
        $output .= '<span class="btn-icon '.$btn_animation.'"><i class="'.$icon.'"></i></span>';
      }
    }
    
    $output .= '</a>';
  }

  $output .= '</div>';

  return $output;

}
