<?php

add_shortcode( 'dylan_blog_summary', 'dylan_blog_summary' );

function dylan_blog_summary( $atts ) {
  extract( shortcode_atts( array(
    'posts_to_show' => '4',
  ), $atts ) );

  $args = array('post_type' => 'post', 'orderby'=> 'date', 'posts_per_page' => $posts_to_show);
  $mainquery = new WP_query($args);
  $output = '<div class="owl-carousel" data-options=\'{"items": 3, "margin": 50, "autoplay": true, "dots": true}\'>';
    
    if( $mainquery->have_posts()) :
      while ($mainquery->have_posts()) : $mainquery->the_post();
        $output .= '<div id="post-'.get_the_id().'" class="card-post '.implode(' ', get_post_class()).'">';
        if (has_post_thumbnail(get_the_id())) {
          $image = get_post_thumbnail_id(get_the_id());
          $image_src = wp_get_attachment_image_src($image, 'dylan_blog_thumb');

          $output .= '<div class="post-media">';
          $output .= '<a href="'.esc_url(get_the_permalink()).'">';
          $output .= '<img src="'.esc_url($image_src[0]).'" alt="'.esc_attr(get_the_title()).'">';
          $output .= '</a>';
          $output .= '</div>';
        }
        $output .= '<div class="post-body">';
        $output .= '<span class="post-time">'.get_the_time('F d, Y').'</span>';
        $output .= '<h3><a href="'.get_the_permalink().'">'.esc_attr(get_the_title()).'</a></h3>';
        $output .= '<p class="serif">'.dylan_excerpt(15).'</p>';
        $output .= '<div class="post-info upper">';
        $output .= '<a class="small-link black-text" href="'.get_the_permalink().'"><span>'.__('Read More', 'dylan_addons').'</span> <i class="hc-arrow-right"></i></a>';        
        $output .= '</div>';        
        $output .= '</div>';
        $output .= '</div>';
      endwhile;
    
    endif;
  wp_reset_postdata();

  $output .= '</div>';

  return $output;

}
