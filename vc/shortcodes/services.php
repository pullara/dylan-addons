<?php

add_shortcode( 'dylan_services', 'dylan_services' );

function dylan_services( $atts, $content ) {
  extract( shortcode_atts( array(
    'content'   => !empty($content)? $content : ''
  ), $atts ) );

  $output = '<div class="services">';
  $output .= '<div class="row">';
  $output .= wpb_js_remove_wpautop($content);
  $output .= '</div>';
  $output .= '</div>';
  
  return $output;
}

add_shortcode( 'dylan_single_service', 'dylan_single_service' );

function dylan_single_service( $atts ) {
  extract( shortcode_atts( array(
    'title' => '',
    'text'  => '',
    'icon_type' => 'etline',
    'icon_etline'  => '',
    'icon'  => '',
    'column_width' => 'vc_col-md-6',
    'column_width_sm' => 'vc_col-sm-6',
    'column_width_xs' => 'vc_col-xs-12',
  ), $atts ) );

  $column_class = implode(' ', array($column_width, $column_width_sm, $column_width_xs));  
  $icon = $icon;

  $output = '<div class="'.$column_class.'">';
  $output .= '<div class="service">';
  if ($icon) {
    $output .= '<div class="service-icon">';
    $output .= '<i class="'.esc_attr($icon).'"></i>';
    $output .= '<h4>'.esc_attr($title).'</h4>';
    $output .= '</div>';  
  }
  $output .= '<div class="service-content">';
  $output .= '<p>'.esc_attr($text).'</p>';
  $output .= '</div>';
  $output .= '</div>';
  $output .= '</div>';
  
  return $output;
}


?>
