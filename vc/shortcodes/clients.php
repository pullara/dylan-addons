<?php

add_shortcode( 'dylan_clients', 'dylan_clients' );

function dylan_clients( $atts ) {
  extract( shortcode_atts( array(
    'column_width' => 'col-sm-4 col-xs-6',
    'images' => ''
  ), $atts ) );

  $clients = explode(',', $images);

  $output = '';
  if ($clients) {
    foreach ($clients as $client) {
      
      $column_class = $column_width;

      $output .= '<div class="'.$column_class.'">';
      $output .= '<div class="client-image">';
      $output .= '<img src="'.esc_url(wp_get_attachment_url($client)).'" alt="">';
      $output .= '</div>';
      $output .= '</div>';

    }
  }
  
  return $output;

}
