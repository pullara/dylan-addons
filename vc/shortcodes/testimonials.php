<?php

add_shortcode( 'dylan_testimonials', 'dylan_testimonials' );

function dylan_testimonials( $atts ) {
  extract( shortcode_atts( array(
    'clients' => '',
    'alignment' => 'center',
  ), $atts ) );

  $clients = vc_param_group_parse_atts($clients);

  $align = ($alignment != 'center') ? 'align-'.$alignment : '';

  $output = '<div id="testimonials-slider" data-options=\'{"animation": "slide", "controlNav": true}\' class="flexslider nav-outside '.$align.'">';
  $output .= '<ul class="slides">';
  if ($clients) {
    foreach ($clients as $client) {

      $output .= '<li>';
      $output .= '<blockquote>';
      if (isset($client['image'])) {
        $image_src = wp_get_attachment_image_src($client['image'], 'thumb');
        $output .= '<img src="'.esc_url($image_src[0]).'" alt="'.esc_attr($client['name']).'">';
      }
      if (isset($client['comment'])) {
        $output .= '<p class="serif">'.esc_attr($client['comment']).'</p>';
      }
      if (isset($client['name'])) {
        $output .= '<footer>'.esc_attr($client['name']).'</footer>';
      }
      $output .= '</blockquote>';
      $output .= '</li>';

    }
  }
  $output .= '</ul>';
  $output .= '</div>';

  return $output;

}
