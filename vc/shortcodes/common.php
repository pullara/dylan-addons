<?php

/* Overlay */
add_shortcode('dylan_overlay', 'dylan_overlay');
function dylan_overlay($atts, $content = null){
  $output = '<div class="holder-container">';
  $output .= '<div class="centrize">';
  $output .= '<div class="v-center">';
  $output .= wpb_js_remove_wpautop($content);
  $output .= '</div>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/* Play Button */
add_shortcode( 'dylan_play_button', 'dylan_play_button' );
function dylan_play_button( $atts ) {
  extract( shortcode_atts( array(
    'add_image' => '',
    'image' => '',
    'url'  => '',
  ), $atts ) );

  $output = '';

  switch ($add_image) {
    case 'yes':
      $output .= '<div class="dylan-video-box">';
      $output .= wp_get_attachment_image($image, '');
      $output .= '<div class="dylan-video-box_overlay">';
      $output .= '<div class="centrize">';
      $output .= '<div class="v-center">';
      $output .= '<div data-src="'.esc_url($url).'" class="play-button"><i class="hc-play"></i></div>';
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';
      break;
    
    default:
      $output .= '<div data-src="'.esc_url($url).'" class="play-button"><i class="hc-play"></i></div>';
      break;
  }

  return $output;
}

/* Map */
add_shortcode( 'dylan_map', 'dylan_map' );
function dylan_map( $atts ) {
  extract( shortcode_atts( array(
    'lat'  => '40.773328',
    'lng'  => '-73.960088',
    'title' => '',
  ), $atts ) );

  return '<div id="map" class="full-width" data-title="'.esc_attr($title).'" data-lat="'.esc_attr($lat).'" data-long="'.esc_attr($lng).'"></div>';
}

/* Job Offer */
add_shortcode( 'dylan_job_offer', 'dylan_job_offer' );

function dylan_job_offer( $atts ) {
  extract( shortcode_atts( array(
    'title' => '',
    'category' => '',
    'location' => '',
    'text' => '',
    'link' => '',
  ), $atts ) );

  $button = vc_build_link($link);

  $output = '<div class="job-offer">';

  $output .= '<div class="job-info">';
  $output .= '<div class="row">';

  $output .= '<div class="col-sm-8">';
  $output .= '<span class="upper">'.esc_attr($category).'</span>';
  $output .= '<h3>'.esc_attr($title).'</h3>';
  $output .= '<small class="upper">'.esc_attr($location).'</small>';
  $output .= '</div>';

  $output.= '<div class="col-sm-4 txt-sm-right txt-md-right">';
  $output.= '<a href="'.$button['url'].'" class="btn btn-color btn-sm">'.$button['title'].'</a>';
  $output.= '</div>';

  $output .= '</div>';
  $output .= '</div>';

  $output .= '<div class="job-content">';
  $output .= '<p>'.esc_attr($text).'</p>';
  $output.= '<a href="'.$button['url'].'" class="upper small-link">'.__('Read More', 'dylan_addons').'</a>';
  $output .= '</div>';

  $output .= '</div>';

  return $output;
}


/* Countdown */
add_shortcode( 'dylan_countdown', 'dylan_countdown' );
function dylan_countdown( $atts ) {
  extract( shortcode_atts( array(
    'alignment'  => '',
    'date'       => ''
  ), $atts ) );

  $datetime = strftime('%m/%d/%Y %H:%M:%S', strtotime($date));

  $output = '<ul class="nav countdown '.$alignment.'" data-date="'.esc_attr($datetime).'">';
  $output .='<li><span class="days">00</span>'.__('Days', 'dylan_addons').'</li>';
  $output .='<li><span class="hours">00</span>'.__('Hours', 'dylan_addons').'</li>';
  $output .='<li><span class="minutes">00</span>'.__('Minutes', 'dylan_addons').'</li>';
  $output .='<li><span class="seconds">00</span>'.__('Seconds', 'dylan_addons').'</li>';
  $output .= '</ul>';

  return $output;
}

/* Newsletter Form */
add_shortcode( 'dylan_newsletter_form', 'dylan_newsletter_form' );
function dylan_newsletter_form( $atts ) {
  extract( shortcode_atts( array(
    'url'  => '',
  ), $atts ) );


  $output = '<form data-mailchimp="true" class="newsletter-form" data-url="'.esc_url($url).'">';
  $output .= '<div class="input-group">';
  $output .= '<input type="email" name="email" placeholder="'.__('Email', 'dylan_addons').'" class="form-control">';
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn newsletter-btn">'.__('Subscribe', 'dylan_addons').'</button>';
  $output .= '</span>';
  $output .= '</div>';
  $output .= '</form>';

  return $output;
}

/* Alerts */
add_shortcode( 'dylan_alert', 'dylan_alert' );
function dylan_alert( $atts ) {
  extract( shortcode_atts( array(
    'color'   => 'alert-success',
    'style'   => 'alert-outline',
    'text'    => ''
  ), $atts ) );


  $output = '<div role="alert" class="alert '.$color.' alert-dismissible '.$style.'">';
  $output .= '<button type="button" data-dismiss="alert" aria-label="Close" class="close"><i class="hc-close"></i></button>';
  $output .= esc_attr( $text );
  $output .= '</div>';

  return $output;
}

/* Boxes */
add_shortcode( 'dylan_box', 'dylan_box' );

function dylan_box( $atts, $content ) {
  extract( shortcode_atts( array(
    'style' => '',
    'bg_color' => '',    
    'box_shadow' => '',
    'content'   => !empty($content)? $content : ''
  ), $atts ) );

  $css_class = array('box');

  if ($box_shadow) {
    $css_class[] = $box_shadow;
  }
  
  if ($style != 'outlined') {
    $css_class[] = $bg_color;
  } else{
    $css_class[] = $style;
  }

  $output = '<div class="'.trim(implode(' ', $css_class)).'">';
  $output .= wpb_js_remove_wpautop($content);
  $output .= '</div>';
  
  return $output;
}
