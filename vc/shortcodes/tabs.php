<?php

function dylan_bs_attribute_map($str, $att = null) {
  $res = array();
  $return = array();
  $reg = get_shortcode_regex();
  preg_match_all('~'.$reg.'~',$str, $matches);
  foreach($matches[2] as $key => $name) {
    $parsed = shortcode_parse_atts($matches[3][$key]);
    $parsed = is_array($parsed) ? $parsed : array();
      $res[$name] = $parsed;
      $return[] = $res;
    }
  return $return;
}

/**
* Bootstrap Tabs
*/

class DylanTabs{
  
  function __construct(){
    add_shortcode( 'dylan_tabs', array($this, 'dylan_tabs'));
  }

  function dylan_tabs($atts, $content = null){
    if( isset( $GLOBALS['tabs_count'] ) )
      $GLOBALS['tabs_count']++;
    else
      $GLOBALS['tabs_count'] = 0;

    $GLOBALS['tabs_default_count'] = 0;

    $atts = shortcode_atts( array(
      'style'   => 'boxed-tabs',
      'alignment' => '',
      'css' => '',
      'icon_color' => '',
    ), $atts );

    $atts_map = dylan_bs_attribute_map( $content );

    $ul_class  = 'nav nav-tabs';
    $ul_class .= ' '.$atts['style'];
    $ul_class .= ( isset($atts['alignment']) && $atts['alignment'] != 'left' ) ? ' '. $atts['alignment'].'-tabs' : '';
    if (count($atts_map) <= 6) {
      $ul_class .= ' cols-'.count($atts_map);
    }
      
    $div_class = 'tab-content';
    $div_class .= ( isset($atts['alignment']) && $atts['alignment'] != 'left' ) ? ' text-'. $atts['alignment'] : '';
    
    
    // Extract the tab titles for use in the tab widget.
    if ( $atts_map ) {
      $tabs = array();
      $GLOBALS['tabs_default_active'] = true;
      foreach( $atts_map as $check ) {
        if( !empty($check["vc_tta_section"]["active"]) ) {
          $GLOBALS['tabs_default_active'] = false;
        }
      }
      $i = 0;
      foreach( $atts_map as $tab ) {
        
        $class  ='';
        $class .= ( !empty($tab["vc_tta_section"]["active"]) || ($GLOBALS['tabs_default_active'] && $i == 0) ) ? 'active' : '';
        $class .= ( !empty($tab["vc_tta_section"]["xclass"]) ) ? ' ' . $tab["vc_tta_section"]["xclass"] : '';
        
        $tabs[] = sprintf(
          '<li%s><a href="#%s" data-toggle="tab">%s<span>%s</span></a></li>',
          ( !empty($class) ) ? ' class="' . $class . '"' : '',
          $tab["vc_tta_section"]["tab_id"],
          ( !empty($tab["vc_tta_section"]["icon"]) && $atts['style'] == 'icon-tabs' ) ? '<i class="'.$tab["vc_tta_section"]["icon"].'"></i>' : '',
          $tab["vc_tta_section"]["title"]
        );
        $i++;
      }
    }

    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), 'dylan_tabs', $atts );

    return sprintf( 
      '<div class="dylan-tabs %s"><ul class="%s">%s</ul><div class="%s">%s</div></div>',
      esc_attr( $css_class ),
      esc_attr( $ul_class ),
      ( $tabs )  ? implode( $tabs ) : '',
      esc_attr( $div_class ),
      wpb_js_remove_wpautop( $content )
    );
  }
}

new DylanTabs();
