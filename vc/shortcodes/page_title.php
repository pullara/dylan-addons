<?php

add_shortcode( 'dylan_page_title', 'dylan_page_title' );

function dylan_page_title( $atts ) {
  extract( shortcode_atts( array(
    'title' => '',
    'subtitle' => '',
    'tag' => 'h2',
    'border' => '',
    'text_align' => 'center',
    'text_align_sm' => '',
    'text_align_xs' => '',
    'text_transform' => '',
    'title_dot'  => '1',
    'title_style' => '',
    'subtitle_style' => '',
    'horizontal_rule' => '',
    'show_icon' => '',
    'icon'  => '',
    'hr_color' => '',
    'font_variant' => '',
    'font_weight' => '',
    'subtitle_position' => 'bottom',
    'title_color' => '',
    'subtitle_color' => '',
  ), $atts ) );

  $class_array = array('title');

  if (empty($text_align_sm) && empty($text_align_xs) && $text_align != 'left') {
    $class_array[] = $text_align;
  } elseif ($text_align != 'left') {
    $class_array[] = 'txt-md-'.$text_align;
  }
  
  if (!empty($text_align_xs)) {
    $class_array[] = 'txt-xs-'.$text_align_xs;
  }

  if (!empty($text_align_sm)) {
    $class_array[] = 'txt-sm-'.$text_align_sm;
  }

  $text_class = ($text_transform != 'none') ? $text_transform : '';

  if ($title_style) {
    $text_class .= ' '.$title_style;
  }
  if ($font_variant) {
    $text_class .= ' '.$font_variant;
  }

  if ($font_weight) {
    $text_class .= ' '.$font_weight;
  }

  $title_color_attr = ($title_color != '') ? ' style="color: '.$title_color.'!important"' : '';
  $subtitle_color_attr = ($subtitle_color != '') ? ' style="color: '.$subtitle_color.'!important"' : '';

  $output = '<div class="'.implode(' ', $class_array).'">';
  if ($show_icon == 'yes') {
    $output .= '<i class="'.$icon.'"></i>';
  }

  if ($subtitle && $subtitle_position == 'top') {
    $output .= '<h4 class="'.trim($subtitle_style).'" '.$subtitle_color_attr.'>'.wp_kses( $subtitle, array('br' => array())) .'</h4>';
  }

  $output .= '<'.$tag . ' class="'.trim($text_class) .'" '.$title_color_attr.'>' . wp_kses( $title, array('br' => array(), 'b' => array()));

  $output .= '</'.$tag.'>';

  if ($subtitle && $subtitle_position == 'bottom') {
    $output .= '<h4 class="'.trim($subtitle_style).'" '.$subtitle_color_attr.'>'.wp_kses( $subtitle, array('br' => array())) .'</h4>';
  }

  if ($horizontal_rule == '1') {
    $hr_class = (!empty($hr_color)) ? ' class="'.$hr_color.'"': '';
    $output .= '<hr'.$hr_class.'>';
  }
  $output .= '</div>';

  return $output;
}
