<?php

add_shortcode( 'dylan_home_slider', 'dylan_home_slider' );

function dylan_home_slider( $atts, $content ) {
  extract( shortcode_atts( array(
    'animated' => '',
    'content'   => !empty($content)? $content : ''
  ), $atts ) );

  $animation = ($animated) ? 'data-animated="true"': '';

  $output = '<div id="home-slider" class="flexslider" '.$animation.'>';
  $output .= '<ul class="slides">';
  $output .= wpb_js_remove_wpautop($content);
  $output .= '</ul>';
  $output .= '</div>';
  
  return $output;
}

add_shortcode( 'dylan_home_slide', 'dylan_home_slide' );

function dylan_home_slide( $atts ) {
  extract( shortcode_atts( array(
    'slide_bg' => 'image',
    'image' => '',
    'slide_video' => '',
    'top_text' => '',
    'headline' => '',
    'subtitle' => '',
    'buttons' => '',
    'text_align' => 'center',
    'font_size' => 'big',
    'text_transform' => '',
    'font_style' => '',
    'subtitle_font_style' => '',
    'top_text_font_style' => '',
    'font_variant' => '',
    'font_weight' => '',
    'letter_spacing' => '',
    'overlay_style' => ''
  ), $atts ) );

  $buttons = vc_param_group_parse_atts( $buttons );

  $align_text = ($text_align != 'center') ? 'text-'.$text_align : '';
  
  $heading_class_arr = array($font_variant, $font_style, $font_weight, $letter_spacing);
  if ($text_transform == 'upper') {
    $heading_class_arr[] = 'upper';
  }
  if ($font_size != 'big') {
    $heading_class_arr[] = 'font-'.$font_size;
  }

  $heading_class = 'class="' . trim(implode(' ', $heading_class_arr)) . '"';

  $output = '<li>';
  if ($slide_bg == 'image' && $image) {
    $output .= '<div class="slide-image" style="background-image: url('.esc_attr(wp_get_attachment_url($image)).');"></div>';
  } elseif ($slide_bg == 'video') {
    $output .= '<div class="slide-video">';
    $output .= '<video autoplay loop preload="auto">';
    $output .= '<source src="'.esc_url($slide_video).'" type="video/mp4">';
    $output .= '</video>';
    $output .= '</div>';
  }
  $output .= '<div class="slide-wrap '.$overlay_style.'">';
  $output .= '<div class="slide-content '.$align_text.'">';
  $output .= '<div class="container">';

  if ($top_text) {
    $output .= '<h5 class="mb-15 '.$top_text_font_style.'">'.esc_attr($top_text).'</h5>';
  }
  
  $output .= '<h1 '.$heading_class.'>'.wp_kses( $headline, array('br' => array())) . '</h1>';
  
  if ($subtitle) {
    $output .= '<h5 class="'.$subtitle_font_style.'">'.esc_attr($subtitle).'</h5>';
  }

  if ($buttons) {
    $output .= '<p class="mt-25">';
    foreach ($buttons as $button) {
      if (isset($button['link'])) {

        $icon = (isset($button['icon'])) ? $button['icon'] : '';
        $btn_image = (isset($button['button_image'])) ? $button['button_image'] : '';

        $button_link = vc_build_link($button['link']);
        
        $btn_target = (!empty($button_link['target'])) ? 'target="'.trim($button_link['target']).'"' : '';
        $shape = (isset($button['shape'])) ? $button['shape'] : '';
        $btn_custom_color = ($button['color'] == 'custom' && $button['custom_color'] != '') ? 'style="border-color: '.$button['custom_color'].'; background-color: '.$button['custom_color'].';"' : '';

        $play_btn = ($button['color'] != 'image' && isset($button['play_button']) && $button['play_button'] == 'yes') ? 'data-play-button="true"' : '';

        $output .= '<a '.$play_btn.' class="btn btn-'.$button['color']. ' ' .$shape. '" href="'.esc_url($button_link['url']).'" '.$btn_target.' '.$btn_custom_color.'>';

        if ($button['color'] == 'image' && $btn_image) {
          $output .= '<img src="'.wp_get_attachment_url($btn_image).'">';
        } else{

          if ($btn_custom_color) {
            $output .= '<span>';
          }

          $output .= esc_attr($button_link['title']);

          if ($btn_custom_color) {
            $output .= '</span>';
          }

          if (isset($button['show_icon']) && $button['show_icon'] != '') {
            $btn_animation = ($icon == 'hc-angle-right' || $icon == 'hc-arrow-right') ? 'btn-icon-animated' : '';
            $output .= '<span class="btn-icon '.$btn_animation.'"><i class="'.$icon.'"></i></span>';
          }

        }
        $output .= '</a>';

      }
    }
    $output .= '</p>';
  }
  $output .= '</div>';
  $output .= '</div>';
  $output .= '</div>';
  $output .= '</li>';

  return $output;
}
?>
