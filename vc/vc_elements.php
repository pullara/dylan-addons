<?php

$portfolio_array = array();
$portfolio_posts = get_posts(array('posts_per_page' => '-1', 'post_type' => 'portfolio'));
foreach ($portfolio_posts as $post) {
  $portfolio_array[$post->post_title] = $post->ID;
}

/* Page Title */
vc_map(
  array(
    'name' => __('Page Title'),
    'base' => 'dylan_page_title',
    'icon' => 'ti-uppercase',
    'description' => __('Styled heading.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'textarea',
        'value' => '',
        'heading' => __('Title', 'dylan_addons'),
        'param_name' => 'title',
        'admin_label' => true
      ),
      array(
        'type' => 'textarea',
        'value' => '',
        'heading' => __('Subtitle', 'dylan_addons'),
        'param_name' => 'subtitle',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'H1'    => 'h1',
          'H2'    => 'h2',
          'H3'    => 'h3',
          'H4'    => 'h4',
          'H5'    => 'h5',
        ),
        'heading' => __('Title Tag', 'dylan_addons'),
        'param_name' => 'tag',
        'std' => 'h2'
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'No' => '',
          'Yes'     => 'yes',
        ),
        'heading' => __('Add Icon?', 'dylan_addons'),
        'param_name' => 'show_icon',
        'std' => '',
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',
        'settings' => array(
          'type' => 'hodyicons',
          'emptyIcon' => false,
          'iconsPerPage' => 100
        ),
        'dependency' => array(
          'element' => 'show_icon',
          'value' => array('yes')
        ),
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Top'    => 'top',
          'Bottom'  => 'bottom',
        ),
        'heading' => __('Subtitle Position', 'dylan_addons'),
        'param_name' => 'subtitle_position',
        'std' => 'bottom',
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'    => '',
          'Serif'  => 'serif',
          'Cursive'   => 'cursive'
        ),
        'heading' => __('Title Font Style', 'dylan_addons'),
        'param_name' => 'title_style',
        'std' => '',
        'group' => __('Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15', 
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'    => '',
          'Serif'  => 'serif',
          'Cursive'   => 'cursive'
        ),
        'heading' => __('Subtitle Font Style', 'dylan_addons'),
        'param_name' => 'subtitle_style',
        'std' => '',
        'group' => __('Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15', 
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => __('Text Align', 'dylan_addons'),
        'param_name' => 'text_align',
        'std' => 'center',
        'edit_field_class' => 'vc_col-xs-4 m-15',        
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          '' => '',
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => __('Text Align (Tablet)', 'dylan_addons'),
        'param_name' => 'text_align_sm',
        'std' => '',
        'edit_field_class' => 'vc_col-xs-4 m-15',        
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          '' => '',
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => __('Text Align (Smartphone)', 'dylan_addons'),
        'param_name' => 'text_align_xs',
        'std' => '',
        'edit_field_class' => 'vc_col-xs-4 m-15',        
      ),      
      array(
        'type' => 'dropdown',
        'value' => array(
          'No'  => '0',
          'Yes' => '1',
        ),
        'heading' => __('Show Horizontal Rule?', 'dylan_addons'),
        'param_name' => 'horizontal_rule',
        'group' => __('Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15', 
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Horizontal Rule Color', 'dylan_addons'),
        'param_name' => 'hr_color',
        'value' => array(
          'Black'  => '',
          'Colored' => 'colored',
          'White' => 'white'
        ),
        'dependency' => array(
          'element' => 'horizontal_rule',
          'value' => array('1')
        ),
        'group' => __('Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15', 
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'None' => 'none',
          'Uppercase'   => 'upper',
        ),
        'heading' => __('Text Transform', 'dylan_addons'),     
        'param_name' => 'text_transform',
        'std' => 'none',
        'group' => __('Style', 'dylan_addons')    
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'   => '',
          'Small Caps'  => 'font-variant-small-caps',
        ),        
        'heading' => __('Font Variant', 'dylan_addons'),
        'param_name' => 'font_variant',        
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Bold'   => '',
          'Normal' => 'font-weight-400',
        ),        
        'heading' => __('Font Weight', 'dylan_addons'),
        'param_name' => 'font_weight',        
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'colorpicker',
        'heading' => __( 'Title Color', 'dylan_addons'),
        'value' => '',
        'param_name' => 'title_color',
        'edit_field_class' => 'vc_col-xs-6 m-15', 
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'colorpicker',
        'heading' => __( 'Subtitle Color', 'dylan_addons'),
        'value' => '',
        'param_name' => 'subtitle_color',
        'edit_field_class' => 'vc_col-xs-6 m-15', 
        'group' => __('Style', 'dylan_addons')
      ),
    )
  )
);
  
/* Home Slider */  
vc_map(
  array(
    'name' => __('Home Slider', 'dylan_addons'),
    'base' => 'dylan_home_slider',
    'icon' => 'ti-layout-slider',
    'description' => __('Slideshow with images and texts.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'as_parent' => array('only' => 'dylan_home_slide'),
    'content_element' => true,
    'is_container' => true,
    'show_settings_on_create' => false,
    'js_view' => 'VcColumnView'
  )
);

/* Single Home Slider */
vc_map(
  array(
    'name' => __('Single Slide', 'dylan_addons'),
    'base' => 'dylan_home_slide',
    'icon' => 'ti-image',
    'description' => __('Slide with images and text.', 'dylan_addons'),
    'content_element' => true,
    'as_child' => array('only' => 'dylan_home_slider'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Background', 'dylan_addons'),
        'param_name' => 'slide_bg',
        'value' => array(
          'Image' => 'image',
          'Video' => 'video'
        ),
        'group' => __('Background', 'dylan_addons'),
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Dark'    => '',
          'Extra Dark'    => 'extra-dark',
          'Light'  => 'light',
          'Colored' => 'colored',
          'Gradient' => 'gradient',
        ),
        'heading' => __('Overlay Style', 'dylan_addons'),
        'param_name' => 'overlay_style',
        'std' => '',
        'group' => __('Background', 'dylan_addons'),
      ),
      array(
        'type' => 'attach_image',
        'value' => '',
        'heading' => __('Background Image', 'dylan_addons'),
        'param_name' => 'image',
        'dependency' => array(
          'element' => 'slide_bg',
          'value' => 'image'
        ),
        'group' => __('Background', 'dylan_addons'),
      ),
      array(
        'type' => 'attach_video',
        'heading' => __( 'Video', 'dylan_addons' ),
        'param_name' => 'slide_video',
        'value' => '',
        'description' => __( 'Select or upload a video.', 'dylan_addons' ),
        'dependency' => array(
          'element' => 'slide_bg',
          'value' => 'video',
        ),
        'group' => __('Background', 'dylan_addons'),
      ),      
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Top Text', 'dylan_addons'),
        'param_name' => 'top_text',
        'group' => __('Content', 'dylan_addons'),
      ),
      array(
        'type' => 'textarea',
        'value' => '',
        'heading' => __('Headline', 'dylan_addons'),
        'param_name' => 'headline',
        'group' => __('Content', 'dylan_addons'),
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Subtitle', 'dylan_addons'),
        'param_name' => 'subtitle',
        'group' => __('Content', 'dylan_addons'),
      ),
      array(
        'type' => 'param_group',
        'value' => '',
        'param_name' => 'buttons',
        'heading' => __('Buttons', 'dylan_addons'),
        'group' => __('Content', 'dylan_addons'),
        'params' => array(
          array(
            'type' => 'vc_link',
            'value' => '',
            'heading' => __('Button Link', 'dylan_addons'),
            'param_name' => 'link',
          ),
          array(
            'type' => 'dropdown',
            'heading' => __('Button Style', 'dylan_addons'),
            'param_name' => 'color',
            'value' => array(
              'Colored' => 'color',
              'Colored (borders only)' => 'color-out',
              'Dark' => 'dark',
              'Dark (borders only)' => 'dark-out',
              'Light' => 'light',
              'Light (borders only)' => 'light-out',
              'Button With Image' => 'image',
              'Custom' => 'custom'
            )
          ),
          array(
            'type' => 'attach_image',
            'heading' => __( 'Button Image', 'dylan_addons'),
            'value' => '',
            'param_name' => 'button_image',
            'dependency' => array(
              'element' => 'color',
              'value' => array('image')
            )
          ),
          array(
            'type' => 'colorpicker',
            'heading' => __( 'Custom Button Color', 'dylan_addons'),
            'value' => '',
            'param_name' => 'custom_color',
            'dependency' => array(
              'element' => 'color',
              'value' => array('custom')
            )
          ),
          array(
            'type' => 'dropdown',
            'param_name' => 'shape',
            'heading' => __('Border Shape', 'dylan_addons'),
            'value' => array(
              'Default'  => '',              
              'Circle' => 'btn-circle'
            ),
            'std' => '',
            'dependency' => array(
              'element' => 'color',
              'value' => array('color', 'color-out', 'dark', 'dark-out', 'light', 'light-out', 'custom')
            )
          ),
          array(
            'type' => 'dropdown',
            'value' => array(
              'No' => '',
              'Yes'     => 'yes',
            ),
            'heading' => __('Add Icon?', 'dylan_addons'),
            'param_name' => 'show_icon',
            'std' => '',
            'dependency' => array(
              'element' => 'color',
              'value' => array('color', 'color-out', 'dark', 'dark-out', 'light', 'light-out', 'custom')
            )
          ),
          array(
            'type' => 'iconpicker',
            'heading' => __('Icon', 'dylan_addons'),
            'param_name' => 'icon',
            'settings' => array(
              'type' => 'hodyicons',
              'emptyIcon' => false,
              'iconsPerPage' => 100
            ),
            'dependency' => array(
              'element' => 'show_icon',
              'value' => array('yes')
            ),
          ),
          array(
            'type' => 'dropdown',
            'param_name' => 'play_button',
            'heading' => __( 'Open Modal Video on click?', 'dylan_addons'),
            'value' => array(
              'No' => '',
              'Yes' => 'yes',
            ),
            'dependency' => array(
              'element' => 'color',
              'value' => array('color', 'color-out', 'dark', 'dark-out', 'light', 'light-out', 'custom')
            )
          ),
        ),
      ),      
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'   => '',
          'Serif'   => 'serif',
          'Cursive'  => 'cursive',
        ),
        'heading' => __('Heading Font Style', 'dylan_addons'),
        'param_name' => 'font_style',
        'group' => __('Font Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-4 m-15',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'   => '',
          'Serif'   => 'serif',
          'Cursive'  => 'cursive',
        ),
        'heading' => __('Subtitle Font Style', 'dylan_addons'),
        'param_name' => 'subtitle_font_style',
        'group' => __('Font Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-4 m-15',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'   => '',
          'Serif'   => 'serif',
          'Cursive'  => 'cursive',
        ),
        'heading' => __('Top Text Font Style', 'dylan_addons'),
        'param_name' => 'top_text_font_style',
        'group' => __('Font Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-4 m-15',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => __('Text Align', 'dylan_addons'),
        'param_name' => 'text_align',
        'std' => 'center',
        'group' => __('Font Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Small'   => 'small',
          'Medium'  => 'medium',
          'Big'     => 'big'
        ),
        'std' => 'big',
        'heading' => __('Font Size', 'dylan_addons'),
        'param_name' => 'font_size', 
        'group' => __('Font Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15',         
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'None' => '',
          'Uppercase'   => 'upper',
        ),
        'heading' => __('Text Transform', 'dylan_addons'),     
        'param_name' => 'text_transform',
        'group' => __('Font Style', 'dylan_addons'),
        'std' => '',
        'edit_field_class' => 'vc_col-xs-6 m-15',       
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'   => '',
          'Small Caps'  => 'font-variant-small-caps',
        ),        
        'heading' => __('Font Variant', 'dylan_addons'),
        'param_name' => 'font_variant',        
        'group' => __('Font Style', 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 m-15',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Bold'   => '',
          'Normal' => 'font-weight-400',
        ),        
        'heading' => __('Font Weight', 'dylan_addons'),
        'param_name' => 'font_weight',        
        'group' => __('Font Style', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Normal' => '',
          'Large' => 'lts-large',
        ),        
        'heading' => __('Letter Spacing', 'dylan_addons'),
        'param_name' => 'letter_spacing',        
        'group' => __('Font Style', 'dylan_addons'),
        'dependency' => array(
          'element' => 'text_transform',
          'value' => array('upper'),
        ),
      ),      
    )
  )
);

/* Text Rotator */
vc_map(
  array(
    'name' => __('Text Rotator'),
    'base' => 'dylan_text_rotator',
    'description' => __('Simple text rotator.', 'dylan_addons'),
    'icon' => 'ti-layout-media-center',
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'value' => array(
          'None' => '',
          'Uppercase'   => 'upper',
        ),
        'heading' => __('Text Transform', 'dylan_addons'),     
        'param_name' => 'text_transform',
        'std' => ''       
      ),
      array(
        'type' => 'param_group',
        'value' => '',
        'param_name' => 'headings',
        'heading' => __('Headings', 'dylan_addons'),
        'params' => array(
          array(
            'type' => 'textfield',
            'value' => '',
            'heading' => __('Heading', 'dylan_addons'),
            'param_name' => 'heading',
          )
        )
      )
    )
  )
);

/* Buttons */
vc_map(
  array(
    'name' => __('Button', 'dylan_addons'),
    'base' => 'dylan_button',
    'icon' => 'ti-control-play',
    'description' => __('Button element.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),    
    'params' => array(
      array(
        'type' => 'textfield',
        'param_name' => 'text',
        'heading' => __('Text', 'dylan_addons'),
        'value' => ''
      ),
      array(
        'type' => 'vc_link',
        'param_name' => 'link',
        'heading' => __('URL (Link)', 'dylan_addons'),
        'value' => ''
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'style',
        'heading' => __('Style', 'dylan_addons'),
        'value' => array(
          'Colored' => 'color',
          'Colored (borders only)' => 'color-out',
          'Dark' => 'dark',
          'Dark (borders only)' => 'dark-out',
          'Light' => 'light',
          'Light (borders only)' => 'light-out',
          'Button With Image' => 'image',
          'Custom' => 'custom',
        ),
        'std' => 'color'
      ),
      array(
        'type' => 'attach_image',
        'heading' => __( 'Button Image', 'dylan_addons'),
        'value' => '',
        'param_name' => 'button_image',
        'dependency' => array(
          'element' => 'style',
          'value' => array('image')
        )
      ),
      array(
        'type' => 'colorpicker',
        'heading' => __( 'Custom Button Color', 'dylan_addons'),
        'value' => '',
        'param_name' => 'custom_color',
        'dependency' => array(
          'element' => 'style',
          'value' => array('custom')
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'alignment',
        'heading' => __('Alignment', 'dylan_addons'),
        'value' => array(
          'Inline'  => 'inline-btn-container',
          'Center'  => 'text-center',
          'Left'    => 'text-left',
          'Right'   => 'text-right'
        ),
        'std' => 'inline-btn-container'
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'shape',
        'heading' => __('Shape', 'dylan_addons'),
        'value' => array(
          'Rounded' => '',
          'Square'  => 'btn-square',
        ),
        'std' => ''
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'size',
        'heading' => __('Size', 'dylan_addons'),
        'value' => array(
          'Small'  => 'btn-sm',
          'Normal' => 'normal',
          'Big' => 'btn-lg'
        ),
        'std' => 'normal'
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'No' => '',
          'Yes'     => 'yes',
        ),
        'heading' => __('Add Icon?', 'dylan_addons'),
        'param_name' => 'show_icon',
        'std' => '',
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',
        'settings' => array(
          'type' => 'hodyicons',
          'emptyIcon' => false,
          'iconsPerPage' => 100
        ),
        'group' => __('Style', 'dylan_addons'),
        'dependency' => array(
          'element' => 'show_icon',
          'value' => 'yes'
        )
      ),
    ) 
  )
);

/* Services */
vc_map(
  array(
    'name' => __('Services Box', 'dylan_addons'),
    'base' => 'dylan_services',
    'icon' => 'ti-layout-grid2',
    'description' => __('Services container', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'as_parent' => array('only' => 'dylan_single_service'),
    'content_element' => true,
    'is_container' => true,
    'show_settings_on_create' => false,
    'js_view' => 'VcColumnView'
  )
);

/* Single Service */
vc_map(
  array(
    'name' => __('Service', 'dylan_addons'),
    'base' => 'dylan_single_service',
    'icon' => 'ti-layout-media-overlay-alt-2',
    'content_element' => true,
    'as_child' => array('only' => 'dylan_services'),
    'params' => array(
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',
        'settings' => array(
          'type' => 'hodyicons',
          'emptyIcon' => false,
          'iconsPerPage' => 100
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Title', 'dylan_addons'),
        'admin_label' => true,
        'param_name' => 'title',
      ),
      array(
        'type' => 'textarea',
        'value' => '',
        'heading' => __('Text', 'dylan_addons'),
        'param_name' => 'text',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Column Width', 'dylan_addons'),
        'param_name' => 'column_width',
        'group' => __( 'Design Options', 'dylan_addons'),
        'value' => array(
          '1 column - 1/12' => 'vc_col-md-1',
          '2 columns - 1/6' => 'vc_col-md-2',
          '3 columns - 1/4' => 'vc_col-md-3',
          '4 columns - 1/3' => 'vc_col-md-4',
          '5 columns - 5/12' => 'vc_col-md-5',
          '6 columns - 1/2' => 'vc_col-md-6',
          '7 columns - 7/12' => 'vc_col-md-7',
          '8 columns - 2/3' => 'vc_col-md-8',
          '9 columns - 3/4' => 'vc_col-md-9',
          '10 columns - 5/6' => 'vc_col-md-10',
          '11 columns - 11/12' => 'vc_col-md-11',
          '12 columns - 1/1' => 'vc_col-md-12',
        ),
        'std' => 'vc_col-md-6'
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Column Width (Tablet)', 'dylan_addons'),
        'param_name' => 'column_width_sm',
        'group' => __( 'Design Options', 'dylan_addons'),
        'value' => array(
          '1 column - 1/12' => 'vc_col-sm-1',
          '2 columns - 1/6' => 'vc_col-sm-2',
          '3 columns - 1/4' => 'vc_col-sm-3',
          '4 columns - 1/3' => 'vc_col-sm-4',
          '5 columns - 5/12' => 'vc_col-sm-5',
          '6 columns - 1/2' => 'vc_col-sm-6',
          '7 columns - 7/12' => 'vc_col-sm-7',
          '8 columns - 2/3' => 'vc_col-sm-8',
          '9 columns - 3/4' => 'vc_col-sm-9',
          '10 columns - 5/6' => 'vc_col-sm-10',
          '11 columns - 11/12' => 'vc_col-sm-11',
          '12 columns - 1/1' => 'vc_col-sm-12',
        ),
        'std' => 'vc_col-sm-6'
      ),
      array(
        'type' => 'dropdown',
        'heading' => __( 'Column Width (Smartphone)', 'dylan_addons'),
        'param_name' => 'column_width_xs',
        'group' => __( 'Design Options', 'dylan_addons'),
        'value' => array(
          '1 column - 1/12' => 'vc_col-xs-1',
          '2 columns - 1/6' => 'vc_col-xs-2',
          '3 columns - 1/4' => 'vc_col-xs-3',
          '4 columns - 1/3' => 'vc_col-xs-4',
          '5 columns - 5/12' => 'vc_col-xs-5',
          '6 columns - 1/2' => 'vc_col-xs-6',
          '7 columns - 7/12' => 'vc_col-xs-7',
          '8 columns - 2/3' => 'vc_col-xs-8',
          '9 columns - 3/4' => 'vc_col-xs-9',
          '10 columns - 5/6' => 'vc_col-xs-10',
          '11 columns - 11/12' => 'vc_col-xs-11',
          '12 columns - 1/1' => 'vc_col-xs-12',
        ),
        'std' => 'vc_col-xs-12'
      ),
    )    
  )
);

/* Features Boxes */
vc_map(
  array(
    'name' => __('Feature Box', 'dylan_addons'),
    'base' => 'dylan_icon_box',
    'category' => __('Content', 'dylan_addons'),
    'icon' => 'ti-star',
    'description' => __('Styled icon with text.', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __( 'Style', 'dylan_addons'),
        'value' => array(
          'Default Icon Box' => 'default',
          'Basic Icon Box' => 'basic',
          'Text Box' => 'text_box',
          'Number Box' => 'number_box'
        ),
        'admin_label' => true,
        'param_name' => 'box_style',
        'description' => __( 'Select icon box style.', 'dylan_addons'),
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Number', 'dylan_addons'),
        'param_name' => 'number',
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('number_box')
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Title', 'dylan_addons'),        
        'param_name' => 'title',
      ),
      array(
        'type' => 'textarea',
        'value' => '',
        'heading' => __('Text', 'dylan_addons'),
        'param_name' => 'text',
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('default', 'basic', 'number_box')
        )
      ),
      array(
        'type' => 'textarea_html',
        'value' => '',
        'heading' => __('Content', 'dylan_addons'),
        'param_name' => 'content',
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('text_box')
        )
      ),
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',        
        'settings' => array(
          'type' => 'hodyicons', 
          'emptyIcon' => false,           
          'iconsPerPage' => 100
        ),
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('default', 'basic')
        )
      ),
      array(
        'type' => 'colorpicker',
        'heading' => __( 'Icon Color', 'dylan_addons'),
        'value' => '',
        'param_name' => 'icon_color',
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('default', 'small', 'simple', 'basic')
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'text_box_style',
        'heading' => __( 'Style', 'dylan_addons'),
        'value' => array(
          'Default' => '',
          'Boxed' => 'boxed',
          'Outlined' => 'outlined'
        ),
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('text_box'),
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'icon_position',
        'heading' => __( 'Icon Position', 'dylan_addons'),
        'value' => array(
          'Left' => '',
          'Right' => 'icon-right',
        ),
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('basic'),
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'icon_background',
        'heading' => __( 'Enable Colored Icon Background?', 'dylan_addons'),
        'value' => array(
          'Yes' => 'yes',
          'No' => 'no',
        ),
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('default'),
        )
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Center'  => '',
          'Left'    => 'align-left',
          'Right'   => 'align-right'
        ),
        'heading' => __('Alignment', 'dylan_addons'),
        'param_name' => 'alignment',
        'std' => 'center',
        'dependency' => array(
          'element' => 'box_style',
          'value' => array('default'),
        ),
      ),
    )
  )
);

/* Clients */
vc_map(
  array(
    'name' => __('Clients', 'dylan_addons'),
    'base' => 'dylan_clients',
    'icon' => 'ti-id-badge',
    'description' => __('Clients logos.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'param_name' => 'column_width',
        'heading' => __('Columns Width', 'dylan_addons'),
        'value' => array(
          '3 Column Desktop / 6 Columns Smartphone' => 'col-sm-3 col-xs-6',
          '4 Column Desktop / 6 Columns Smartphone' => 'col-sm-4 col-xs-6',
          '6 Columns' => 'col-xs-6',
        ),
        'std' => 'col-sm-4 col-xs-6'
      ),
      array(
        'type' => 'attach_images',
        'value' => '',
        'heading' => __('Client Logos', 'dylan_addons'),
        'param_name' => 'images',
      ),
    )
  )
);

/* Testimonials */
vc_map(
  array(
    'name' => __('Testimonials', 'dylan_addons'),
    'base' => 'dylan_testimonials',
    'icon' => 'ti-book',
    'description' => __('Testimonials slider.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'value' => array(
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => __('Alignment', 'dylan_addons'),
        'param_name' => 'alignment',
        'std' => 'center',
      ),
      array(
        'type' => 'param_group',
        'value' => '',
        'param_name' => 'clients',
        'heading' => __('Testimonials', 'dylan_addons'),
        'params' => array(
          array(
            'type' => 'attach_image',
            'value' => '',
            'heading' => __('Testimonial Image', 'dylan_addons'),
            'param_name' => 'image',
          ),
          array(
            'type' => 'textfield',
            'value' => '',
            'heading' => __('Testimonial name', 'dylan_addons'),
            'param_name' => 'name',
            'admin_label' => true
          ),
          array(
            'type' => 'textarea',
            'value' => '',
            'heading' => __('Comment', 'dylan_addons'),
            'param_name' => 'comment',
          ),
        )
      )
    )
  )
);

/* Blog Summary */
vc_map(
  array(
    'name' => __('Blog Summary', 'dylan_addons'),
    'base' => 'dylan_blog_summary',
    'icon' => 'ti-notepad',
    'category' => __('Content', 'dylan_addons'),
    'description' => __('A section with your latest posts.', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'textfield',
        'param_name' => 'posts_to_show',
        'heading' => __('Number of posts to show', 'dylan_addons'),
        'value' => '4'
      )
    )
  )
);

/* Portfolio Section */
vc_map(
  array(
    'name' => __('Portfolio Section', 'dylan_addons'),
    'base' => 'dylan_portfolio',
    'icon' => 'ti-briefcase',
    'category' => __('Content', 'dylan_addons'),
    'description' => __('A section with your portfolio items.', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'param_name' => 'items',
        'heading' => __('Items to show', 'dylan_addons'),
        'value' => array(
          'Latest' => 'latest',
          'Select Manually' => 'manual'
        ),
      ),
      array(
        'type' => 'textfield',
        'param_name' => 'items_to_show',
        'heading' => __('Number of items to show', 'dylan_addons'),
        'value' => '8',
        'dependency' => array(
          'element' => 'items',
          'value' => 'latest'
        )
      ),
      array(
        'type' => 'dropdown_multi',
        'param_name' => 'items_ids',
        'heading' => __('Select Items', 'dylan_addons'),
        'description' => __('Ctrl+click or Cmd+click to select multiple items.', 'dylan_addons'),
        'value' => $portfolio_array,
        'dependency' => array(
          'element' => 'items',
          'value' => 'manual'
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'style',
        'heading' => __('Style', 'dylan_addons'),
        'value' => array(
          'Grid' => 'grid',
          'Carousel' => 'carousel',
        ),
        'std' => ''
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'columns',
        'heading' => __('How many columns?', 'dylan_addons'),
        'value' => array(
          'Two' => 'two-col',
          'Three' => 'three-col',
          'Four' => 'four-col',
        ),
        'std' => 'two-col'
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'show_title',
        'heading' => __('Show title?', 'dylan_addons'),
        'value' => array(
          'Yes' => '1',
          'No'  => '0'
        ),
        'std' => '0',
        'dependency' => array(
          'element' => 'style',
          'value' => array('grid')
        )
      ),
      array(
        'type' => 'textfield',
        'param_name' => 'title',
        'heading' => __('Section title', 'dylan_addons'),
        'value' => '',
        'dependency' => array(
          'element' => 'show_title',
          'value' => '1'
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'filters',
        'heading' => __('Show filters?', 'dylan_addons'),
        'value' => array(
          'Yes' => '1',
          'No'  => '0'
        ),
        'std' => '1',
        'dependency' => array(
          'element' => 'style',
          'value' => array('grid')
        )
      ),
    )
  )
);

/* Accordion */
vc_map(
  array(
    'name' => __('Accordion', 'dylan_addons'),
    'base' => 'dylan_accordion',
    'icon' => 'ti-layout-accordion-merged',
    'description' => __('Collapsible panels.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Behavior', 'dylan_addons'),
        'param_name' => 'behavior',
        'value' => array(
          'Open One at Time' => '',
          'Multiple Open' => 'multiple'
        ),
        'std' => '',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Open First Item Automatically?', 'dylan_addons'),
        'param_name' => 'open_first',
        'value' => array(
          'Yes' => 'yes',
          'No' => 'no'
        ),
        'std' => 'yes',
      ),
      array(
        'type' => 'param_group',
        'param_name' => 'items',
        'heading' => __('Items', 'dylan_addons'),
        'params' => array(
          array(
            'type' => 'textfield',
            'param_name' => 'title',
            'heading' => __('Title', 'dylan_addons'),
            'value' => ''
          ),
          array(
            'type' => 'textarea',
            'param_name' => 'text',
            'heading' => __('Text', 'dylan_addons'),
            'value' => ''
          ),
        )
      ),
    )
  )
);

/* Progress Bars */
vc_map(
  array(
    'name' => __( 'Progress Bar', 'dylan_addons'),
    'base' => 'dylan_progress_bar',
    'icon' => 'ti-align-left',
    'description' => __('Animated Progress Bar.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'param_name' => 'style',
        'heading' => __('Style', 'dylan_addons'),
        'value' => array(
          'Line' => '',
          'Circle' => 'circle'
        )
      ),
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',
        'settings' => array(
          'type' => 'hodyicons',
          'emptyIcon' => true,
          'iconsPerPage' => 100
        ),
        'dependency' => array(
          'element' => 'style',
          'value' => array('circle')
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'bg_color',
        'heading' => __('Progress Bar Color', 'dylan_addons'),
        'value' => array(
          'Black' => 'black',
          'Colored' => 'colored',
          'White' => 'white'
        )
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Name', 'dylan_addons'),
        'param_name' => 'name',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Percentage', 'dylan_addons'),
        'param_name' => 'percentage',
      )
    )
  )
);

/* Team Member */
vc_map(
  array(
    'name' => __( 'Team Member', 'dylan_addons'),
    'base' => 'dylan_team_member',
    'icon' => 'ti-user',
    'description' => __('Add a team member.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Style', 'dylan_addons'),
        'param_name' => 'style',
        'value' => array(
          'Default' => '',
          'Round Thumbnail' => 'round-thumb',
          'Image + Overlay' => 'overlay',
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Thumb Size', 'dylan_addons'),
        'param_name' => 'thumb_size',
        'value' => array(
          'Small' => '',
          'Big' => 'big-thumb',
        ),
        'dependency' => array(
          'element' => 'style',
          'value' => array('round-thumb'),
        ),
      ),
      array(
        'type' => 'attach_image',
        'value' => '',
        'heading' => __('Image', 'dylan_addons'),
        'param_name' => 'image',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Name', 'dylan_addons'),
        'param_name' => 'name',
        'admin_label' => true
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Role', 'dylan_addons'),
        'param_name' => 'role',
      ),
      array(
        'type' => 'textarea',
        'value' => '',
        'heading' => __('Description', 'dylan_addons'),
        'param_name' => 'description',
      ),
      array(
        'type' => 'param_group',
        'heading' => __('Social Links', 'dylan_addons'),
        'param_name' => 'socials',
        'params' => array(
          array(
            'type' => 'dropdown',
            'heading' => __('Social', 'dylan_addons'),
            'param_name' => 'social',
            'admin_label' => true,
            'value' => array(
              'Facebook' => 'facebook',
              'Twitter' => 'twitter',
              'Linkedin' => 'linkedin',
              'Instagram' => 'instagram',
              'Dribbble' => 'dribbble',
              'Github' => 'github',
              'Flickr' => 'flickr',
              'Pinterest' => 'pinterest',
              'YouTube' => 'youtube',
              'Tumblr' => 'tumblr'
            )
          ),
          array(
            'type' => 'textfield',
            'value' => '',
            'heading' => __('URL', 'dylan_addons'),
            'param_name' => 'url',
          ),
        )
      ),
    )
  )
);

/* Carousel */
vc_map(
  array(
    'name' => __('Carousel', 'dylan_addons'),
    'base' => 'dylan_carousel',
    'icon' => 'ti-layout-slider-alt',
    'description' => __('Animated carousel.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'is_container' => true,
    'params' => array(
      array(
        'type' => 'textfield',
        'value' => '4',
        'heading' => __('Items', 'dylan_addons'),
        'description' => __('Set the maximum amount of items displayed at a time.', 'dylan_addons'),
        'param_name' => 'items',
      ),
      array(
        'type' => 'textfield',
        'value' => '10',
        'heading' => __('Margin', 'dylan_addons'),
        'description' => __('Set the margin between items.', 'dylan_addons'),
        'param_name' => 'margin',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Loop', 'dylan_addons'),
        'param_name' => 'loop',
        'value' => array(
          'On' => '1',
          'Off'  => '',
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Autoplay', 'dylan_addons'),
        'param_name' => 'autoplay',
        'value' => array(
          'On' => '1',
          'Off'  => '',
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Controls', 'comet'),
        'param_name' => 'controls',
        'value' => array(
          'Off'  => '',
          'On' => '1',
        ),
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Items (medium devices)', 'dylan_addons'),
        'description' => __('Set the maximum amount of items displayed at a time on medium devices.', 'dylan_addons'),
        'param_name' => 'md_items',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Items (small devices)', 'dylan_addons'),
        'description' => __('Set the maximum amount of items displayed at a time on small devices.', 'dylan_addons'),
        'param_name' => 'sm_items',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Items (extra small devices)', 'dylan_addons'),
        'description' => __('Set the maximum amount of items displayed at a time on extra small devices.', 'dylan_addons'),
        'param_name' => 'xs_items',
      ),
      array(
        'type' => 'css_editor',
        'heading' => __( 'CSS box', 'js_composer' ),
        'param_name' => 'css',
        'group' => __( 'Design Options', 'js_composer' ),
      ),
    ),
    'js_view' => 'VcColumnView'
  )
);

/* Pricing Tables */
vc_map(
  array(
    'name' => __('Pricing Table', 'dylan_addons'),
    'base' => 'dylan_pricing_table',
    'icon' => 'ti-money',
    'description' => __('Pricing Table.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'value' => array(
          'No' => '',
          'Yes'     => 'yes',
        ),
        'heading' => __('Add Icon?', 'dylan_addons'),
        'param_name' => 'show_icon',
        'std' => '',
      ),
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',
        'settings' => array(
          'type' => 'hodyicons',
          'emptyIcon' => false,
          'iconsPerPage' => 100
        ),
        'dependency' => array(
          'element' => 'show_icon',
          'value' => array('yes')
        )
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Alignment', 'dylan_addons'),
        'param_name' => 'alignment',
        'value' => array(
          'Center' => '',
          'Left' => 'align-left',
        ),
        'std' => '',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Title', 'dylan_addons'),
        'param_name' => 'title',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Price', 'dylan_addons'),
        'param_name' => 'price',
      ),
      array(
        'type' => 'textfield',
        'value' => '$',
        'heading' => __('Currency', 'dylan_addons'),
        'param_name' => 'currency',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Style', 'dylan_addons'),
        'param_name' => 'style',
        'value' => array(
          'Default' => '',
          'Hightlight' => 'featured'
        ),
        'std' => ''
      ),
      array(
        'type' => 'param_group',
        'heading' => __('Features', 'dylan_addons'),
        'param_name' => 'features',
        'params' => array(
          array(
            'type' => 'textfield',
            'heading' => __('Text', 'dylan_addons'),
            'param_name' => 'text',
            'value' => ''
          )
        )
      ),
      array(
        'type' => 'vc_link',
        'value' => '',
        'heading' => __('Button', 'dylan_addons'),
        'param_name' => 'button',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Button Color', 'dylan_addons'),
        'param_name' => 'button_color',
        'value' => array(
          'Colored' => 'color',
          'Colored (borders only)' => 'color-out',
          'Dark' => 'dark',
          'Dark (borders only)' => 'dark-out',
          'Light' => 'light',
          'Light (borders only)' => 'light-out',
        )
      ),      
    )
  )
);

/* Photo Gallery */
vc_map(
  array(
    'name' => __('Photo Gallery', 'dylan_addons'),
    'base' => 'dylan_photo_gallery',
    'icon' => 'ti-gallery',
    'description' => __('Modal photo gallery.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'param_name' => 'layout',
        'heading' => __('Layout', 'dylan_addons'),
        'value' => array(
          'Default' => '',
          'With Margin' => 'with-margin'
        ),
      ),
      array(
        'type' => 'param_group',
        'param_name' => 'photos',
        'heading' => __('Photos', 'dylan_addons'),
        'value' => '',
        'params' => array(
          array(
            'type' => 'attach_image',
            'param_name' => 'pic',
            'admin_label' => true,
            'heading' => __('Image', 'dylan_addons'),
            'value' => ''
          ),
          array(
            'type' => 'dropdown',
            'param_name' => 'size',
            'heading' => __('Image Size', 'dylan_addons'),
            'value' => array(
              'Default' => '',
              'Half Width' => 'half'
            )
          ),
          array(
            'type' => 'textfield',
            'heading' => __('Caption', 'dylan_addons'),
            'param_name' => 'caption',
            'value' => ''
          )
        )
      )
    )
  )
);

/* Image Slider */
vc_map(
  array(
    'name' => __('Image Slider', 'dylan_addons'),
    'base' => 'dylan_image_slider',
    'icon' => 'ti-image',
    'description' => __('Image Slider.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'param_group',
        'param_name' => 'images',
        'heading' => __('Images', 'dylan_addons'),
        'value' => '',
        'params' => array(
          array(
            'type' => 'attach_image',
            'param_name' => 'pic',
            'heading' => __('Select Image', 'dylan_addons'),
            'value' => ''
          ),
          array(
            'type' => 'textfield',
            'heading' => __('Title', 'dylan_addons'),
            'param_name' => 'title',
            'value' => ''
          ),
          array(
            'type' => 'textarea',
            'heading' => __('Text', 'dylan_addons'),
            'param_name' => 'text',
            'value' => ''
          ),
          array(
            'type' => 'vc_link',
            'heading' => __('Link', 'dylan_addons'),
            'param_name' => 'link',
            'value' => ''
          )
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'animation',
        'heading' => __('Animation', 'dylan_addons'),
        'value' => array(
          'Fade' => '',
          'Slide' => 'slide'
        ),
        'group' => __('Settings', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'control_nav',
        'heading' => __('Control Nav', 'dylan_addons'),
        'value' => array(
          'On' => '',
          'Off' => 'off'
        ),
        'group' => __('Settings', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'direction_nav',
        'heading' => __('Direction Nav', 'dylan_addons'),
        'value' => array(
          'On' => '',
          'Off' => 'off'
        ),
        'group' => __('Settings', 'dylan_addons')
      ),
    )
  )
);

/* Restaurant Menu */
vc_map(
  array(
    'name' => __('Menu', 'dylan_addons'),
    'base' => 'dylan_menu',
    'icon' => 'ti-menu',
    'description' => __('Menu with prices or info.', 'dylan_addons'),
    'category' => __( 'Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'value' => array(
          'List'    => '',
          'Grid'  => 'boxes',
        ),
        'heading' => __('Style', 'dylan_addons'),
        'param_name' => 'style',
        'std' => '',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'    => '',
          'Serif'  => 'serif',
          'Cursive'   => 'cursive'
        ),
        'heading' => __('Font Style', 'dylan_addons'),
        'param_name' => 'font_style',
        'std' => '',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'   => '',
          'Small Caps'  => 'font-variant-small-caps',
        ),        
        'heading' => __('Font Variant', 'dylan_addons'),
        'param_name' => 'font_variant',        
      ),
      array(
        'type' => 'param_group',
        'heading' => __('Menu Items', 'dylan_addons'),
        'param_name' => 'menu_items',
        'params' => array(
          array(
            'type' => 'dropdown',
            'heading' => __('Add Image?', 'dylan_addons'),
            'param_name' => 'add_image',
            'value' => array(
              'No' => '',
              'Yes' => 'yes'
            ),
            'std' => '',
          ),
          array(
            'type' => 'attach_image',
            'value' => '',
            'heading' => __('Image', 'dylan_addons'),
            'param_name' => 'image',
            'dependency' => array(
              'element' => 'add_image',
              'value' => 'yes'
            ),
          ),
          array(
            'type' => 'textfield',
            'param_name' => 'title',
            'value' => '',
            'admin_label' => true,
            'heading' => __('Title', 'dylan_addons'),
          ),          
          array(
            'type' => 'textfield',
            'param_name' => 'infoline',
            'value' => '',
            'heading' => __('Infoline', 'dylan_addons'),
            'description' => __('I.E: $7.99', 'dylan_addons')
          ),
          array(
            'type' => 'textarea',
            'param_name' => 'text',
            'value' => '',
            'heading' => __('Text', 'dylan_addons'),
          ),
          array(
            'type' => 'dropdown',
            'value' => array(
              'No' => '',
              'Yes'     => 'yes',
            ),
            'heading' => __('Add Link?', 'comet'),
            'param_name' => 'add_link',
            'std' => '',
          ),
          array(
            'type' => 'vc_link',
            'value' => '',
            'heading' => __('Link', 'comet'),
            'param_name' => 'link',
            'dependency' => array(
              'element' => 'add_link',
              'value' => array('yes'),
            ),
          ),
        )
      ),
    )
  )
);

/* Counter */
vc_map(
  array(
    'name' => __('Counter', 'dylan_addons'),
    'base' => 'dylan_counter',
    'icon' => 'ti-signal',
    'description' => __('Animated Counter.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Number', 'dylan_addons'),
        'param_name' => 'number'
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Text', 'dylan_addons'),
        'param_name' => 'text'
      ),
      array(
        'type' => 'iconpicker',
        'heading' => __('Icon', 'dylan_addons'),
        'param_name' => 'icon',
        'settings' => array(
          'type' => 'hodyicons',
          'emptyIcon' => true,
          'iconsPerPage' => 100
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'icon_color',
        'heading' => __( 'Icon Color', 'dylan_addons'),
        'value' => array(
          'Colored' => '',
          'Black' => 'black',
          'White' => 'white',
        ),
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Icon Position', 'dylan_addons'),
        'param_name' => 'icon_position',
        'value' => array(
          'Left' => '',
          'Top' => 'block'
        ),
        'std' => '',
        'group' => __('Style', 'dylan_addons')
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Default'    => '',
          'Serif'  => 'serif',
          'Cursive'   => 'cursive'
        ),
        'heading' => __('Font Style', 'dylan_addons'),
        'param_name' => 'font_style',
        'std' => '',
        'group' => __('Style', 'dylan_addons')
      ),
    )
  )
);

/* Social Icons */
vc_map(
  array(
    'name' => __('Social Icons', 'dylan_addons'),
    'base' => 'dylan_social_icons',
    'icon' => 'ti-facebook',
    'description' => __('Social media icons.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Style', 'dylan_addons'),
        'param_name' => 'style',
        'value' => array(
          'Default' => '',
          'Minimal' => 'style-2'
        ),
        'std' => '',
      ),
      array(
        'type' => 'dropdown',
        'value' => array(
          'Left'    => 'left',
          'Center'  => 'center',
          'Right'   => 'right'
        ),
        'heading' => __('Align', 'dylan_addons'),
        'param_name' => 'icons_align',
        'std' => 'center',
      ),
      array(
        'type' => 'param_group',
        'heading' => __('Social Links', 'dylan_addons'),
        'param_name' => 'socials',
        'params' => array(
          array(
            'type' => 'dropdown',
            'heading' => __('Social', 'dylan_addons'),
            'param_name' => 'social',
            'admin_label' => true,
            'value' => array(
              '' => '',
              'Facebook' => 'facebook',
              'Twitter' => 'twitter',
              'Linkedin' => 'linkedin',
              'Instagram' => 'instagram',
              'Dribbble' => 'dribbble',
              'Github' => 'github',
              'Flickr' => 'flickr',
              'Pinterest' => 'pinterest',
              'YouTube' => 'youtube',
              'Tumblr' => 'tumblr'
            )
          ),
          array(
            'type' => 'textfield',
            'value' => '',
            'heading' => __('URL', 'dylan_addons'),
            'param_name' => 'url',
          ),
        )
      )
    )
  )
);

/* Timeline */
vc_map(
  array(
    'name' => __('Timeline', 'dylan_addons'),
    'base' => 'dylan_timeline',
    'icon' => 'ti-layout-list-thumb-alt',
    'description' => __('Timeline with your resume details.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'param_group',
        'param_name' => 'items',
        'heading' => __('Items', 'dylan_addons'),
        'params' => array(
          array(
            'type' => 'textfield',
            'param_name' => 'title',
            'heading' => __('Title', 'dylan_addons'),
            'value' => ''
          ),
          array(
            'type' => 'textarea',
            'param_name' => 'text',
            'heading' => __('Text', 'dylan_addons'),
            'value' => ''
          ),
          array(
            'type' => 'textfield',
            'param_name' => 'place',
            'heading' => __('Job Place/University', 'dylan_addons'),
            'value' => ''
          ),
          array(
            'type' => 'textfield',
            'param_name' => 'date',
            'heading' => __('Date', 'dylan_addons'),
            'description' => __('I.E: February 2010 - April 2012', 'dylan_addons'),
            'value' => ''
          )
        )
      )
    )
  )
);

/* Map */
vc_map(
  array(
    'name' => __('Google Maps', 'dylan_addons'),
    'base' => 'dylan_map',
    'icon' => 'ti-location-pin',
    'description' => __('Map block.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'textfield',
        'value' => '40.773328',
        'heading' => __('Map Latitude', 'dylan_addons'),
        'param_name' => 'lat',
        "group" => __( "Dylan Options", 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6',
        'description' => __('Find your Latitude and Longitude <a target="blank" href="http://www.latlong.net/">here</a>', 'dylan_addons'),
      ),
      array(
        'type' => 'textfield',
        'value' => '-73.960088',
        'heading' => __('Map Longitude', 'dylan_addons'),
        'param_name' => 'lng',
        "group" => __( "Dylan Options", 'dylan_addons'),
        'edit_field_class' => 'vc_col-xs-6 pt-0',
      ),
      array(
        'type' => 'textfield',
        'value' => '',
        'heading' => __('Title', 'dylan_addons'),
        'param_name' => 'title',
        "group" => __( "Dylan Options", 'dylan_addons'),
      ),
    )
  )
);

/* Countdown */
vc_map(
  array(
    'name' => __('Countdown', 'dylan_addons'),
    'base' => 'dylan_countdown',
    'icon' => 'ti-timer',
    'description' => __('Animated countdown.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Alignment', 'dylan_addons'),
        'param_name' => 'alignment',
        'value' => array(
          'Left' => '',
          'Center' => 'text-center'
        ),
        'std' => '',
      ),
      array(
        'type' => 'datepicker',
        'param_name' => 'date',
        'heading' => __('Date', 'dylan_addons'),
        'value' => ''
      ),
    )
  )
);

/* Newsletter form */
vc_map(
  array(
    'name' => __('Newsletter Form', 'dylan_addons'),
    'base' => 'dylan_newsletter_form',
    'icon' => 'ti-email',
    'description' => __('Mailchimp newsletter form.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'textfield',
        'param_name' => 'url',
        'heading' => __('URL', 'dylan_addons'),
        'value' => '',
        'description' => __('Add your Mailchimp URL. It should look like this: <b>http://hody.us12.list-manage.com/subscribe/post?u=d9d989sc1b2ba80926372a9fb&id=c789sg65b0</b>', 'dylan_addons')
      ),
    )
  )
);

/* Modal Play Button */
vc_map(
  array(
    'name' => __('Play Button', 'dylan_addons'),
    'base' => 'dylan_play_button',
    'icon' => 'ti-control-play',
    'description' => __('A modal video', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Add Image?', 'dylan_addons'),
        'param_name' => 'add_image',
        'value' => array(
          'No' => '',
          'Yes' => 'yes'
        ),
        'std' => '',
      ),
      array(
        'type' => 'attach_image',
        'value' => '',
        'heading' => __('Image', 'dylan_addons'),
        'param_name' => 'image',
        'dependency' => array(
          'element' => 'add_image',
          'value' => 'yes'
        ),
      ),
      array(
        'type' => 'textfield',
        'param_name' => 'url',
        'heading' => __('URL', 'dylan_addons'),
        'value' => '',
        'description' => __('Add your video IFRAME URL. It should look like this: <b>https://www.youtube.com/embed/r44RKWyfcFw?autoplay=1</b>', 'dylan_addons')
      ),
    )
  )
);

/* Box */
vc_map(
  array(
    'name' => __('Box', 'dylan_addons'),
    'base' => 'dylan_box',
    'icon' => 'ti-package',
    'description' => __('A cointainer for elements.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'content_element' => true,
    'is_container' => true,
    'params' => array(
      array(
        'type' => 'dropdown',
        'heading' => __('Style', 'dylan_addons'),
        'param_name' => 'style',
        'value' => array(
          'Boxed' => '',
          'Outlined' => 'outlined'
        ),
        'std' => '',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Background Color', 'dylan_addons'),
        'param_name' => 'bg_color',
        'value' => array(
          'White' => '',
          'Grey'  => 'grey-bg',
          'Black' => 'dark-bg',
          'Colored' => 'colored-bg',
        ),
        'dependency' => array(
          'element' => 'style',
          'is_empty' => true,
        ),
        'std' => '',
      ),
      array(
        'type' => 'dropdown',
        'heading' => __('Box Shadow', 'dylan_addons'),
        'param_name' => 'box_shadow',
        'value' => array(
          'Disabled' => '',
          'Enabled' => 'with-box-shadow'
        ),
        'std' => '',
      ),
    ),
    'js_view' => 'VcColumnView'
  )
);

/* Alerts */
vc_map(
  array(
    'name' => __('Alert', 'dylan_addons'),
    'base' => 'dylan_alert',
    'icon' => 'ti-info-alt',
    'description' => __('Notification element.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'param_name' => 'color',
        'heading' => __('Color', 'dylan_addons'),
        'value' => array(
          'Green' => 'alert-success',
          'Blue' => 'alert-info',
          'Yellow' => 'alert-warning',
          'Red' => 'alert-danger',
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'style',
        'heading' => __('Style', 'dylan_addons'),
        'value' => array(
          'Outlined' => 'alert-outline',
          'Colored' => 'alert-colored',
        )
      ),
      array(
        'type' => 'textarea',
        'param_name' => 'text',
        'heading' => __('Text', 'dylan_addons'),
        'value' => ''
      ),
    )
  )
);

/* Tabs */
vc_map(
  array(
    'name' => __( 'Tabs', 'dylan_addons'),
    'base' => 'dylan_tabs',
    'icon' => 'ti-layout-tab',
    'allowed_container_element' => 'vc_row',
    'is_container' => true,    
    'as_parent' => array(
      'only' => 'vc_tta_section',
    ),
    'description' => __('Tabbed contents.', 'dylan_addons'),
    'category' => __('Content', 'dylan_addons'),
    'description' => __( 'Tabbed content', 'dylan_addons'),
    'params' => array(
      array(
        'type' => 'dropdown',
        'param_name' => 'style',
        'value' => array(
          __( 'Boxed', 'dylan_addons') => 'boxed-tabs',
          __( 'Minimal', 'dylan_addons') => 'minimal-tabs',
          __( 'With Icon', 'dylan_addons') => 'icon-tabs',
        ),
        'heading' => __( 'Style', 'dylan_addons'),
        'description' => __( 'Select tabs display style.', 'dylan_addons'),
      ),
      array(
        'type' => 'colorpicker',
        'heading' => __( 'Active Icon Color', 'dylan_addons'),
        'value' => '',
        'param_name' => 'icon_color',
        'dependency' => array(
          'element' => 'style',
          'value' => array('icon-tabs')
        )
      ),
      array(
        'type' => 'dropdown',
        'param_name' => 'alignment',
        'value' => array(
          __( 'Left', 'dylan_addons') => 'left',
          __( 'Center', 'dylan_addons') => 'center',
          __( 'Right', 'dylan_addons') => 'right',
        ),
        'heading' => __( 'Alignment', 'dylan_addons'),
        'description' => __( 'Select tabs section title alignment.', 'dylan_addons'),
      ),
      array(
        'type' => 'css_editor',
        'heading' => __( 'Css', 'dylan_addons' ),
        'param_name' => 'css',
        'group' => __( 'Design options', 'dylan_addons' ),
      ),
    ),
    'default_content' => '',
    'js_view' => 'VcBackendTtaTabsView',
    'custom_markup' => '
<div class="vc_tta-container" data-vc-action="collapse">
  <div class="vc_general vc_tta vc_tta-tabs vc_tta-color-backend-tabs-white vc_tta-style-flat vc_tta-shape-rounded vc_tta-spacing-1 vc_tta-tabs-position-top vc_tta-controls-align-left">
    <div class="vc_tta-tabs-container">'
                     . '<ul class="vc_tta-tabs-list">'
                     . '<li class="vc_tta-tab" data-vc-tab data-vc-target-model-id="{{ model_id }}" data-element_type="dylan_single_tab"><a href="javascript:;" data-vc-tabs data-vc-container=".vc_tta" data-vc-target="[data-model-id=\'{{ model_id }}\']" data-vc-target-model-id="{{ model_id }}"><span class="vc_tta-title-text">{{ section_title }}</span></a></li>'
                     . '</ul>
    </div>
    <div class="vc_tta-panels vc_clearfix {{container-class}}">
      {{ content }}
    </div>
  </div>
</div>',
    'default_content' => '
[vc_tta_section title="' . sprintf( '%s %d', __( 'Tab', 'js_composer' ), 1 ) . '"][/vc_tta_section]
[vc_tta_section title="' . sprintf( '%s %d', __( 'Tab', 'js_composer' ), 2 ) . '"][/vc_tta_section]
  ',
    'admin_enqueue_js' => array(
      vc_asset_url( 'lib/vc_tabs/vc-tabs.min.js' ),
    ),
  )
);

if (class_exists('WPBakeryShortCodesContainer')) {
  class WPBakeryShortCode_Dylan_Services extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Dylan_Single_Service extends WPBakeryShortCode {}
  class WPBakeryShortCode_Dylan_Home_Slider extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Dylan_Box extends WPBakeryShortCodesContainer {}
  class WPBakeryShortCode_Dylan_Carousel extends WPBakeryShortCodesContainer {}
}

VcShortcodeAutoloader::getInstance()->includeClass( 'WPBakeryShortCode_VC_Tta_Tabs' );

if (class_exists('WPBakeryShortCode_VC_Tta_Tabs')) {
  class WPBakeryShortCode_Dylan_Tabs extends WPBakeryShortCode_VC_Tta_Tabs {}
}

if (class_exists('WPBakeryShortCode')) {
  class WPBakeryShortCode_Dylan_Home_Slide extends WPBakeryShortCode {}
}
